#include "servosequence.h"
#include "armmanager.h"
#include <QPropertyAnimation>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QPauseAnimation>
#define M_PI 3.141592653589793238462643

ServoState::ServoState(QObject *parent) :
    QObject(parent)
{
    m_servoValues.resize(ARMIO_MAX_SERVOS_NUM);
    for(int i = 0; i < m_servoValues.size(); i++)
        m_servoValues[i] = std::nanf("");
}

void ServoState::dumpCurrentState()
{
    for(int i = 0; i < ArmManager::instance()->servoCount(); i++) {
        ServoConfig* conf = ArmManager::instance()->configAt(i);
        if(!conf)
            setValue(i, std::nanf(""));
        else
            setValue(i, ArmManager::instance()->servoAngle(i));
    }
}

void ServoState::dumpZeroState()
{
    for(int i = 0; i < ArmManager::instance()->servoCount(); i++) {
        ServoConfig* conf = ArmManager::instance()->configAt(i);
        if(!conf)
            setValue(i, std::nanf(""));
        else
            setValue(i, 0);
    }
}

void ServoState::setValue(int id, float value)
{
    if((id < 0) || (id >= m_servoValues.size())) {
        Q_ASSERT(false);
        return;
    }
    m_servoValues[id] = value;
}

float ServoState::value(int id) const
{
    if((id < 0) || (id >= m_servoValues.size())) {
        Q_ASSERT(false);
        return std::nanf("");
    }
    return m_servoValues[id];
}

bool ServoState::hasValue(int id) const
{
    if((id < 0) || (id >= m_servoValues.size())) {
        Q_ASSERT(false);
        return false;
    }
    return !std::isnan(m_servoValues[id]);
}

int ServoState::valueCount() const
{
    return m_servoValues.size();
}

QString ServoState::name() const
{
    return m_name;
}

void ServoState::setName(const QString &name)
{
    m_name = name;
}

//-------------------------------------------------------------------------------

ServoStateAnimation::ServoStateAnimation(QObject *parent) :
    QParallelAnimationGroup(parent)
{

}

void ServoStateAnimation::createFromStates(ServoState *state1, ServoState *state2)
{
    if(!state1 || !state2) {
        Q_ASSERT(false);
        return;
    }

    this->clear();

    qint64 longest_time = 0;
    for(int i = 0; i < state1->valueCount(); i++) {
        ServoConfig* conf = ArmManager::instance()->configAt(i);
        if(!conf) continue;

        if(state1->hasValue(i) && state2->hasValue(i)) {
            QPropertyAnimation* anim = new QPropertyAnimation(this);
            anim->setTargetObject(ArmManager::instance()->handler(i));
            anim->setPropertyName("angle");
            anim->setStartValue(QVariant::fromValue(state1->value(i)));
            anim->setEndValue(QVariant::fromValue(state2->value(i)));
            qint64 anim_time = conf->calculateTransitionTimeMs(state1->value(i), state2->value(i), 0);
            longest_time = std::max(longest_time, anim_time);
//            this->addAnimation(new QPauseAnimation(1000));
        }
        else qCritical() << "Animation error!";
    }

    for(int i = 0; i < animationCount(); i++) {
        QPropertyAnimation* anim = qobject_cast<QPropertyAnimation*>(animationAt(i));
        if(anim)
            anim->setDuration(longest_time);
    }
}

void ServoStateAnimation::createFromState(ServoState *state)
{
    if(!state) {
        Q_ASSERT(false);
        return;
    }

    this->clear();

    qint64 longest_time = 0;
    for(int i = 0; i < state->valueCount(); i++) {
        ServoConfig* conf = ArmManager::instance()->configAt(i);
        if(!conf) continue;
        if(!state->hasValue(i)) continue;

        QPropertyAnimation* anim = new QPropertyAnimation(this);
        anim->setTargetObject(ArmManager::instance()->handler(i));
        anim->setPropertyName("angle");
        anim->setStartValue(QVariant::fromValue(state->value(i)));
        anim->setEndValue(QVariant::fromValue(state->value(i)));
        anim->setEasingCurve(m_easing);

        qint64 anim_time = conf->calculateTransitionTimeMs(-M_PI/2.0, 0, 0);
        longest_time = std::max(longest_time, anim_time);
//        this->addAnimation(new QPauseAnimation(1000));
    }

    for(int i = 0; i < animationCount(); i++) {
        QPropertyAnimation* anim = qobject_cast<QPropertyAnimation*>(animationAt(i));
        if(anim)
            anim->setDuration(longest_time);
    }
}

void ServoStateAnimation::setEasingCurve(const QEasingCurve &curve)
{
    m_easing = curve;
    for(int i = 0; i < animationCount(); i++) {
        QPropertyAnimation* anim = qobject_cast<QPropertyAnimation*>(animationAt(i));
        if(anim)
            anim->setEasingCurve(curve);
    }
}

QEasingCurve ServoStateAnimation::easingCurve() const
{
    return m_easing;
}

//----------------------------------------------------------------------------

ServoSequence::ServoSequence(QObject *parent)
{
}

ServoState* ServoSequence::addCurrentState()
{
    ServoState* state = new ServoState(this);
    state->dumpCurrentState();
    addState(state);
    return state;
}

ServoState* ServoSequence::addZeroState()
{
    ServoState* state = new ServoState(this);
    state->dumpZeroState();
    addState(state);
    return state;
}

void ServoSequence::addState(ServoState *state)
{
    if(!m_states.contains(state)) {
        m_states.append(state);
        emit statesChanged();
    }
}

void ServoSequence::removeState(ServoState *state)
{
    if(m_states.removeAll(state)) {
        if(state->parent() == this)
            delete state;
        emit statesChanged();
    }
}

void ServoSequence::removeAllStates()
{
    for(int i = 0; i < m_states.size(); i++) {
        if(m_states[i]->parent() == this)
            delete m_states[i];
    }
    m_states.clear();
    emit statesChanged();
}

ServoStateAnimation *ServoSequence::addTransition(ServoState *nextState)
{
    if(!m_states.contains(nextState))
        return nullptr;

    int acount = animationCount();
    ServoStateAnimation* anim = new ServoStateAnimation(this);
    if(!m_lastState || !acount)
        anim->createFromState(nextState);
    else
        anim->createFromStates(m_lastState, nextState);
    anim->setEasingCurve(m_easingCurve);

    this->addAnimation(anim);
    this->addAnimation(new QPauseAnimation(1000));

    m_lastState = nextState;
    emit animationsChanged();
}

const ServoState *ServoSequence::stateAt(int idx) const
{
    return m_states.at(idx);
}

ServoState *ServoSequence::stateAt(int idx)
{
    return m_states[idx];
}

int ServoSequence::stateCount() const
{
    return m_states.size();
}

bool ServoSequence::save(const QString &path) const
{
    QJsonObject obj;
    if(!save(obj)) {
        qCritical() << "Can't save config!";
        return false;
    }

    QJsonDocument doc;
    doc.setObject(obj);

    QFile file(path);
    if(!file.open(QIODevice::WriteOnly)) {
        qCritical() << "File open failed: " << path;
        return false;
    }

    file.write(doc.toJson());
    file.close();
    return true;
}

bool ServoSequence::save(QJsonObject &obj) const
{
    QJsonArray states;
    QJsonArray transitions;

    for(int i = 0; i < m_states.size(); i++)
    {
        QJsonObject state;
        state["name"] = m_states[i]->name();

        QJsonArray stateValues;
        for(int j = 0; j < m_states[i]->valueCount(); j++) {
            if(!m_states[i]->hasValue(j))
                continue;
            QJsonObject stateValue;
            stateValue["id"] = j;
            stateValue["value"] = m_states[i]->value(j);
            stateValues.append(stateValue);
        }
        state["values"] = stateValues;
        states.append(state);
    }

    for(int i = 0; i < animationCount(); i++) {
        ServoStateAnimation* anim = qobject_cast<ServoStateAnimation*>(animationAt(i));
        if(!anim)
            continue;

        QJsonArray transition;
        for(int j = 0; j < anim->animationCount(); j++) {
            QPropertyAnimation* propAnim = qobject_cast<QPropertyAnimation*>(anim->animationAt(j));
            if(propAnim) {
                ServoHandler* handler = qobject_cast<ServoHandler*>(propAnim->targetObject());
                QJsonObject propAnimObj;
                propAnimObj["type"] = "QPropertyAnimation";
                propAnimObj["handlerId"] = handler->getId();
                propAnimObj["start"] = propAnim->startValue().toFloat();
                propAnimObj["end"] = propAnim->endValue().toFloat();
                propAnimObj["time"] = propAnim->duration();
                transition.append(propAnimObj);
            }
        }
        transitions.append(transition);
    }

    obj["states"] = states;
    obj["transitions"] = transitions;
    return true;
}

bool ServoSequence::load(const QString &path)
{
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly)) {
        qCritical() << "File open failed: " << path;
        return false;
    }

    QByteArray fileData = file.readAll();
    file.close();
    QJsonDocument doc = QJsonDocument::fromJson(fileData);

    return load(doc.object());
}

bool ServoSequence::load(const QJsonObject &obj)
{
    clear();
    removeAllStates();

    QJsonArray states = obj["states"].toArray();
    QJsonArray transitions = obj["transitions"].toArray();

    for(int i = 0; i < states.size(); i++) {
        QJsonObject stateObj = states[i].toObject();

        ServoState* state = new ServoState(this);
        state->setName(stateObj["name"].toString());

        QJsonArray stateValues = stateObj["values"].toArray();
        for(int j = 0; j < stateValues.size(); j++) {
            QJsonObject stateValue = stateValues[j].toObject();
            int id = stateValue["id"].toInt();
            float angle = stateValue["value"].toDouble();
            state->setValue(id, angle);
        }
        this->addState(state);
    }

    for(int i = 0; i < transitions.size(); i++) {
        ServoStateAnimation* anim = new ServoStateAnimation();
        QJsonArray transition = transitions[i].toArray();

        for(int j = 0; j < transition.size(); j++) {
            QJsonObject animObj = transition[j].toObject();
            if(animObj["type"] == "QPropertyAnimation") {
                QPropertyAnimation* propAnim = new QPropertyAnimation();
                propAnim->setTargetObject(ArmManager::instance()->handler(animObj["handlerId"].toInt()));
                propAnim->setPropertyName("angle");
                propAnim->setStartValue(QVariant::fromValue((float)animObj["start"].toDouble()));
                propAnim->setEndValue(QVariant::fromValue((float)animObj["end"].toDouble()));
                propAnim->setDuration(animObj["time"].toInt());
                anim->addAnimation(propAnim);
            }
        }
        this->addAnimation(anim);
        this->addAnimation(new QPauseAnimation(1000));
    }
    emit statesChanged();
    emit animationsChanged();
    return true;
}

QEasingCurve ServoSequence::easingCurve() const
{
    return m_easingCurve;
}

void ServoSequence::setEasingCurve(const QEasingCurve &easingCurve)
{
    m_easingCurve = easingCurve;

    for(int i = 0; i < animationCount(); i++) {
        ServoStateAnimation* anim = qobject_cast<ServoStateAnimation*>(animationAt(i));
        if(anim)
            anim->setEasingCurve(m_easingCurve);
    }
    emit animationsChanged();
}
