#include "armservo.h"

float ServoConfig::TransitionMult = 1.f;
qint64 ServoConfig::AdditionalTime = 0;

ServoConfig::ServoConfig()
{
    memset(&servo_decl,0, sizeof(ArmMsgData_ServoDecl));
}

ServoConfig::ServoConfig(uint8_t id)
{
    memset(&servo_decl,0, sizeof(ArmMsgData_ServoDecl));
    setServoId(id);
}

void ServoConfig::setServoId(uint8_t id)
{
    servo_decl.servo_id = id;
}

uint8_t ServoConfig::servoId() const
{
    return servo_decl.servo_id;
}

void ServoConfig::setMinPulseWeight(uint16_t val)
{
    servo_decl.min_pulse_weight = val;
}

uint16_t ServoConfig::minPulseWeight() const
{
    return servo_decl.min_pulse_weight;
}

void ServoConfig::setMaxPulseWeight(uint16_t val)
{
    servo_decl.max_pulse_weight = val;
}

uint16_t ServoConfig::maxPulseWeight() const
{
    return servo_decl.max_pulse_weight;
}

void ServoConfig::setPulseFrequency(uint16_t val)
{
    servo_decl.pulse_freq = val;
}

uint16_t ServoConfig::pulseFrequency() const
{
    return servo_decl.pulse_freq;
}

void ServoConfig::setEstimatedSpeed(float fRadPerSec)
{
    servo_decl.estimated_speed = armio::angleToShort(fRadPerSec);
}

float ServoConfig::estimatedSpeed() const
{
    return armio::shortToAngle(servo_decl.estimated_speed);
}

void ServoConfig::setMinimumAngle(float val)
{
    servo_decl.min_angle = armio::angleToShort(val);
}

float ServoConfig::minimumAngle() const
{
    return armio::shortToAngle(servo_decl.min_angle);
}

void ServoConfig::setMaximumAngle(float val)
{
    servo_decl.max_angle = armio::angleToShort(val);
}

float ServoConfig::maximumAngle() const
{
    return armio::shortToAngle(servo_decl.max_angle);
}

bool ServoConfig::saveJSon(QJsonObject &obj)
{
    obj["servo_id"] =           servoId();
    obj["min_pulse_weight"] =   (int)minPulseWeight();
    obj["max_pulse_weight"] =   (int)maxPulseWeight();
    obj["pulse_freq"] =         (int)pulseFrequency();
    obj["estimated_speed"] =    estimatedSpeed();
    obj["min_angle"] =          minimumAngle();
    obj["max_angle"] =          maximumAngle();
    return true;
}

bool ServoConfig::loadJSon(const QJsonObject &obj)
{
    setServoId(obj["servo_id"].toInt());
    setMinPulseWeight(obj["min_pulse_weight"].toInt());
    setMaxPulseWeight(obj["max_pulse_weight"].toInt());
    setPulseFrequency(obj["pulse_freq"].toInt());
    setEstimatedSpeed(obj["estimated_speed"].toDouble());
    setMinimumAngle(obj["min_angle"].toDouble());
    setMaximumAngle(obj["max_angle"].toDouble());
    return true;
}

qint64 ServoConfig::calculateTransitionTimeMs(float ang1, float ang2, qint64 additionalTime)
{
    float dist = fabs(armio::angleNormalizePI(ang2) - armio::angleNormalizePI(ang1));
    return (qint64)((dist / (estimatedSpeed() / 1000.f)) * TransitionMult) + AdditionalTime;
}

const ArmMsgData_ServoDecl &ServoConfig::decl() const
{
    return servo_decl;
}

//void ServoConfig::setCurrentAngle(float fVal)
//{
//    fVal = std::max(fVal, minimumAngle());
//    fVal = std::min(fVal, maximumAngle());

//    if(current_angle != fVal)
//    {
//        current_angle = fVal;
//        emit currentAngleChanged(current_angle);
//    }
//}

//float ServoConfig::currentAngle() const
//{
//    return current_angle;
//}
