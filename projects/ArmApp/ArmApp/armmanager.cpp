#include "armmanager.h"
#include <QDebug>
#include <QFile>
#include <QJsonArray>
#include <QTime>
#include <float.h>


ServoHandler::ServoHandler(int id, QObject *parent) :
    QObject(parent)
{
    this->id = id;
    connect(ArmManager::instance(), &ArmManager::servoAngleChanged,
            this, &ServoHandler::onManagerAngleChanged);
}

void ServoHandler::onManagerAngleChanged(int id, float angle)
{
    if(id == this->id)
        emit angleChanged(angle);
}

int ServoHandler::getId() const
{
    return id;
}

float ServoHandler::getAngle() const
{
    return ArmManager::instance()->servoAngle(id);
}

void ServoHandler::setAngle(float value)
{
    ArmManager::instance()->setServoAngle(id, value, 1);
}

//-----------------------------------------------------

ArmManager *ArmManager::instance()
{
    static ArmManager mgr;
    return &mgr;
}

ArmManager::ArmManager(QObject *parent) : QObject(parent)
{
    time_measure.start();
    prev_time = time_measure.elapsed();

    m_servos.resize(ARMIO_MAX_SERVOS_NUM);
    m_handlers.resize(ARMIO_MAX_SERVOS_NUM);

    m_currentSequence = new ServoSequence(this);

    m_servoMovementTimer = new QTimer(this);
    m_servoMovementTimer->setSingleShot(false);
    m_servoMovementTimer->setInterval(30);
    connect(m_servoMovementTimer, &QTimer::timeout,
            this, &ArmManager::processServo);

}

ArmManager::~ArmManager()
{
    qDebug() << "ArmManager::~ArmManager()";
}

void ArmManager::setConfig(const ServoConfig &config)
{
    if(config.servoId() >= m_servos.size()) {
        Q_ASSERT(false);
        return;
    }

    ServoData* pData = servoDataById(config.servoId());
    if(pData) {
        pData->config = config;
    }
    else {
        pData = new ServoData();
        pData->config = config;
        m_servos[pData->config.servoId()] = pData;
    }

    writeConfig(pData->config.servoId());

    emit configChanged();
}

ServoConfig *ArmManager::configAt(int idx)
{
    ServoData* data = servoDataById(idx);
    if(!data)
        return nullptr;
    return &(data->config);
}

void ArmManager::removeAllConfigs()
{
    for(int i = 0; i < m_servos.size(); i++) {
        if(m_servos.at(i)) {
            delete m_servos[i];
            m_servos[i] = nullptr;
        }
    }
    emit configChanged();
}

ServoHandler *ArmManager::handler(int servoId)
{
    ServoHandler* sh = m_handlers[servoId];
    if(!sh) {
        sh = new ServoHandler(servoId, this);
        m_handlers[servoId] = sh;
    }
    return sh;
}

void ArmManager::enableAllServo()
{
    m_servoMovementTimer->start();
}

void ArmManager::disableAllServo()
{
    m_servoMovementTimer->stop();
}

bool ArmManager::isServoEnabled() const
{
    return m_servoMovementTimer->isActive();
}

void ArmManager::setServoAngle(int id, float angle, qint64 time)
{
    ServoData* sd = servoDataById(id);
    if(sd == nullptr) {
        qDebug() << "Empty servo data!";
        return;
    }

    qDebug() << prev_time << "setServoAngle" << id << armio::radToDeg(angle);

    angle = armio::angleNormalizePI(angle);

    sd->b_first_init = true;
    sd->b_movement_mode = false;
    if(angle > 0)
        sd->target_angle = std::min(angle, sd->config.maximumAngle());
    else
        sd->target_angle = std::max(angle, sd->config.minimumAngle());
    sd->target_speed = fabsf(sd->target_angle - sd->current_angle) * 1000.f / float(time);
    sd->target_speed = ((sd->target_speed < 0) ? -1 : 1) * std::min(fabsf(sd->target_speed), sd->config.estimatedSpeed());
}

void ArmManager::setServoSpeed(int id, float speed)
{
    ServoData* sd = servoDataById(id);
    if(sd == nullptr) {
        qDebug() << "Empty servo data!";
        return;
    }

    sd->b_first_init = true;
    sd->b_movement_mode = true;
    sd->target_speed = ((speed < 0) ? -1 : 1) * std::min(fabsf(speed), sd->config.estimatedSpeed());
}

float ArmManager::servoAngle(int id) const
{
    const ServoData* sd = servoDataById(id);
    if(sd == nullptr)
        return 0.f;
    return sd->current_angle;
}

float ArmManager::servoSpeed(int id) const
{
    const ServoData* sd = servoDataById(id);
    if(sd == nullptr)
        return 0.f;
    return sd->target_speed;
}

void ArmManager::processServo()
{
    qint64 current_time = time_measure.elapsed();
    qint64 delta = m_servoMovementTimer->interval();
    qint64 real_delta = current_time - prev_time;
    QList<int> changed_ids;

    float short_error = armio::getShortError();
    for(int i = 0; i < m_servos.size(); i++)
    {
        ServoData* sd = m_servos[i];
        if(!sd) continue;

        float fffdelta = fabsf(sd->current_angle - sd->next_angle);
        if(fffdelta > short_error) {
            changed_ids.append(i);
        }
        else if(!sd->b_first_init) continue;
//        qDebug() << i << "Delta: " << fffdelta << "Error: " << short_error;

        if(sd->b_first_init) {
            sd->start_time = current_time;
            sd->b_first_init = false;
        }

        sd->estimated_speed = fabsf(sd->next_angle - sd->current_angle) / (float(delta) / 1000.f);
        sd->current_angle = sd->next_angle;

        float ang_delta = sd->target_speed * float(real_delta) / 1000.f;

        if(sd->b_movement_mode)
        {
            sd->next_angle = sd->current_angle + ang_delta;
        }
        else
        {
            float ang_distance = fabsf(sd->target_angle - sd->current_angle);
            ang_distance = std::min(ang_distance, fabsf(ang_delta));
            ang_distance *= (sd->target_angle < sd->current_angle) ? -1.f : 1.f;

            sd->next_angle = sd->current_angle + ang_distance;
        }

        sd->next_angle = armio::angleNormalizePI(sd->next_angle);
        sd->next_angle = armio::angleClamp(sd->next_angle, sd->config.minimumAngle(), sd->config.maximumAngle());

        ArmMessage::Pointer msg = ArmMessage::Create(kArmMsgData_ServoState);
        msg->getData<ArmMsgData_ServoState>(kArmMsgData_ServoState)->angle = armio::angleToShort(sd->next_angle);
        msg->getData<ArmMsgData_ServoState>(kArmMsgData_ServoState)->servo_id = i;
        msg->getData<ArmMsgData_ServoState>(kArmMsgData_ServoState)->speed = sd->target_speed;
        m_connection->sendMessage(msg);

//        qDebug() << real_delta << "Speed error[" << i << "]:" << armio::radToDeg(fabsf(sd->estimated_speed - fabsf(sd->target_speed))) << armio::radToDeg(sd->estimated_speed) << armio::radToDeg(fabsf(sd->target_speed));
    }

    if(!changed_ids.isEmpty()) {
        ArmMessage::Pointer msg = ArmMessage::Create(kArmMsgData_Service);
        msg->getData<ArmMsgData_Service>(kArmMsgData_Service)->code = kServiceCode_ExecuteServo;
        m_connection->sendMessage(msg);
    }

    for(int i = 0; i < changed_ids.size(); i++) {
        int id = changed_ids.at(i);
        emit servoAngleChanged(id,m_servos[id]->current_angle);
    }

    prev_real_delta = real_delta;
    prev_time = current_time;
}

#include <QThread>
void ArmManager::onArmConnected()
{
    for(int i = 0; i < m_servos.size(); i++)
    {
        if(m_servos[i]) {
            writeConfig(i);
//            QThread::usleep(1000);
        }
    }
}

void ArmManager::writeConfig(int id)
{
    if((id < 0) | (id >= m_servos.size())) {
        Q_ASSERT(false);
        return;
    }

    qDebug() << "Transmitting config:" << id;
    ArmMessage::Pointer msg = ArmMessage::Create(kArmMsgData_ServoDecl);
    *(msg->getData<ArmMsgData_ServoDecl>(kArmMsgData_ServoDecl)) = m_servos[id]->config.decl();
    m_connection->sendMessage(msg);
}

ArmManager::ServoData *ArmManager::servoDataById(int id)
{
    if((id < 0) | (id >= m_servos.size())) {
        Q_ASSERT(false);
        return nullptr;
    }

    return m_servos[id];
}

const ArmManager::ServoData *ArmManager::servoDataById(int id) const
{
    if((id < 0) | (id >= m_servos.size())) {
        Q_ASSERT(false);
        return nullptr;
    }
    return m_servos.at(id);
}

ServoSequence *ArmManager::currentSequence() const
{
    return m_currentSequence;
}

ArmConnection *ArmManager::connection() const
{
    return m_connection;
}

void ArmManager::setConnection(ArmConnection *connection)
{
    if(m_connection)
        disconnect(m_connection, 0, this, 0);

    m_connection = connection;

    if(m_connection)
        connect(m_connection, &ArmConnection::connectionEstablished, this, &ArmManager::onArmConnected);
}

bool ArmManager::saveConfig(const QString &path) const
{
    QJsonArray arr;
    if(!saveConfig(arr)) {
        qCritical() << "Can't save config!";
        return false;
    }

    QJsonDocument doc;
    doc.setArray(arr);

    QFile file(path);
    if(!file.open(QIODevice::WriteOnly)) {
        qCritical() << "File open failed: " << path;
        return false;
    }

    file.write(doc.toJson());
    file.close();
    return true;
}

bool ArmManager::saveConfig(QJsonArray &arr) const
{
    for(int i = 0; i < m_servos.size(); i++)
    {
        ServoData* sd = m_servos[i];
        if(!sd) continue;

        QJsonObject obj;
        if(!sd->config.saveJSon(obj))
            return false;
        arr.append(obj);
    }
    return true;
}

bool ArmManager::loadConfig(const QString &path)
{
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly)) {
        qCritical() << "File open failed: " << path;
        return false;
    }

    QByteArray fileData = file.readAll();
    file.close();
    QJsonDocument doc = QJsonDocument::fromJson(fileData);

    return loadConfig(doc.array());
}

bool ArmManager::loadConfig(const QJsonArray &arr)
{
    removeAllConfigs();

    for(int i = 0; i < arr.size(); i++) {
        ServoConfig conf;
        if(!conf.loadJSon(arr.at(i).toObject()))
            return false;
        setConfig(conf);
    }
    return true;
}
