#include "armio_utils.h"
#include <QDebug>
#define M_PI 3.141592653589793238462643

ArmMessage::Pointer ArmMessage::Create(ArmMsgDataType datatype)
{
    return ArmMessage::Pointer(new ArmMessage(datatype));
}

ArmMessage::ArmMessage()
{
    memset(&message, 0, sizeof(message));
}

ArmMessage::ArmMessage(ArmMsgDataType datatype)
{
    memset(&message, 0, sizeof(message));
    create(datatype);
}

ArmMessage::~ArmMessage()
{
    release();
}

void ArmMessage::create(ArmMsgDataType datatype)
{
    release();
    message.magic = ARMIO_MAGIC_NUMBER;
    message.data.pData = CreateArmMsgData(datatype);
    message.data_type = datatype;
    message.data_size = GetArmMsgDataSize(datatype);
}

void ArmMessage::release()
{
    if(message.data.pData) {
        delete message.data.pData;
        message.data.pData = nullptr;
    }
    memset(&message, 0, sizeof(message));
}

bool ArmMessage::createFromByteArray(QByteArray& arr)
{
    release();
    if(arr.size() < ARMIO_HEADER_SIZE) {
        qDebug() << "Header too small: " << arr.size();
        return false;
    }

    memcpy(&message, arr.data(), ARMIO_HEADER_SIZE);

    if(message.magic != ARMIO_MAGIC_NUMBER) {
        qCritical() << "Incorrect magic number:" << message.magic;
        arr.remove(0, ARMIO_HEADER_SIZE);
        return false;
    }

    if(arr.size() < ARMIO_HEADER_SIZE + message.data_size) {
        qDebug() << "Header + data too small: " << arr.size() << message.data_size;
        return false;
    }

    message.data.pData = CreateArmMsgData(message.data_type);
    memcpy(message.data.pData, arr.data() + ARMIO_HEADER_SIZE, message.data_size);
    arr.remove(0, message.data_size + ARMIO_HEADER_SIZE);
    return true;
}

QByteArray ArmMessage::toByteArray() const
{
    QByteArray stream;
    stream.resize(ARMIO_HEADER_SIZE + message.data_size);

    memcpy(stream.data(), &message, ARMIO_HEADER_SIZE);
    memcpy(stream.data() + ARMIO_HEADER_SIZE, message.data.pData, message.data_size);
    return stream;
}

bool ArmMessage::createFromJson(const QJsonObject& obj)
{
    ArmMsgDataType data_type = (ArmMsgDataType)obj["data_type"].toInt();
    if(GetArmMsgDataSize((int)data_type) == 0)
        return false;

    create(data_type);

    switch(message.data_type) {
    case kArmMsgData_ServoDecl:
        message.data.pServoDecl->servo_id =         obj["servo_id"].toInt();
        message.data.pServoDecl->min_pulse_weight = obj["min_pulse_weight"].toInt();
        message.data.pServoDecl->max_pulse_weight = obj["max_pulse_weight"].toInt();
        message.data.pServoDecl->pulse_freq =       obj["pulse_freq"].toInt();
        message.data.pServoDecl->estimated_speed =  obj["estimated_speed"].toInt();
        message.data.pServoDecl->min_angle =        obj["min_angle"].toInt();
        message.data.pServoDecl->max_angle =        obj["max_angle"].toInt();
        break;
    case kArmMsgData_ServoState:
        message.data.pServoState->servo_id =    obj["servo_id"].toInt();
        message.data.pServoState->angle =       obj["angle"].toInt();
        message.data.pServoState->speed =        obj["speed"].toInt();
        break;
    case kArmMsgData_DebugInfo: {
        QByteArray br = obj["debug_info"].toString().toUtf8();
        int size = std::min<int>(254, br.size());
        memcpy(message.data.pDebugCommand->debug_info, br.data(), size);
        break;
    }
    case kArmMsgData_Service:
        message.data.pServiceCommand->code =    (ArmMsgServiceCode)obj["code"].toInt();
        message.data.pServiceCommand->data1 =   obj["data1"].toInt();
        message.data.pServiceCommand->data2 =   obj["data2"].toInt();
        break;
    case kArmMsgData_Echo:
        break;
    default:
        return false;
    }
    return true;
}

bool ArmMessage::toJson(QJsonObject& obj)
{
    switch(message.data_type) {
    case kArmMsgData_ServoDecl:
        obj["data_type"] = kArmMsgData_ServoDecl;
        obj["servo_id"] = message.data.pServoDecl->servo_id;
        obj["min_pulse_weight"] = message.data.pServoDecl->min_pulse_weight;
        obj["max_pulse_weight"] = message.data.pServoDecl->max_pulse_weight;
        obj["pulse_freq"] = message.data.pServoDecl->pulse_freq;
        obj["estimated_speed"] = message.data.pServoDecl->estimated_speed;
        obj["min_angle"] = message.data.pServoDecl->min_angle;
        obj["max_angle"] = message.data.pServoDecl->max_angle;
        break;
    case kArmMsgData_ServoState:
        obj["data_type"] = kArmMsgData_ServoState;
        obj["servo_id"] = message.data.pServoState->servo_id;
        obj["angle"] = message.data.pServoState->angle;
        obj["speed"] = (int)message.data.pServoState->speed;
        break;
    case kArmMsgData_DebugInfo:
        obj["data_type"] = kArmMsgData_DebugInfo;
        obj["debug_info"] = message.data.pDebugCommand->debug_info;
        break;
    case kArmMsgData_Service:
        obj["data_type"] = kArmMsgData_Service;
        obj["code"] = message.data.pServiceCommand->code;
        obj["data1"] = message.data.pServiceCommand->data1;
        obj["data2"] = message.data.pServiceCommand->data2;
        break;
    case kArmMsgData_Echo:
        obj["data_type"] = kArmMsgData_Echo;
        break;
    default:
        return false;
    }
    return true;
}

QString ArmMessage::toString() const
{
    QString data = "UNKNOWN_DATA";
    return data;
}

//----------------------------------------------------
#include <limits>
float armio::angleNormalize2PI(float fRad)
{
    fRad = fmod(fRad,M_PI * 2);
    if (fRad < 0)
        fRad += M_PI * 2;
    return fRad;
}

float armio::angleNormalizePI(float fRad)
{
    fRad = fmod(fRad + M_PI,M_PI * 2);
    if (fRad < 0)
        fRad += M_PI * 2.0;
    return fRad - M_PI;
}

int16_t armio::angleToShort(float fRad)
{
    return (int16_t)((angleNormalizePI(fRad) / M_PI) * std::numeric_limits<int16_t>::max());
}

float armio::shortToAngle(int16_t iVal)
{
    return angleNormalizePI(float(iVal) * M_PI / float(std::numeric_limits<int16_t>::max()));
}

float armio::degToRad(float degrees)
{
    return degrees * M_PI / 180.0;
}

float armio::radToDeg(float radians)
{
    return radians * 180.0 / M_PI;
}

float armio::angleClamp(float fRad, float fMin, float fMax)
{
    return std::max(fMin, std::min(fRad, fMax));
}

float armio::getShortError()
{
    return M_PI / double(std::numeric_limits<int16_t>::max());
}
