#include "servostateframe.h"
#include "ui_servostateframe.h"

ServoStateFrame::ServoStateFrame(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::ServoStateFrame)
{
    ui->setupUi(this);

    connect(ArmManager::instance(), &ArmManager::configChanged, this, &ServoStateFrame::updateServoConfig);
    connect(ArmManager::instance(), &ArmManager::servoAngleChanged, this, &ServoStateFrame::onServoAngleChanged);
}

ServoStateFrame::~ServoStateFrame()
{
    delete ui;
}

void ServoStateFrame::setServoId(int val)
{
    servo_id = val;
    updateServoConfig();
}

QSize ServoStateFrame::sizeHint() const
{
    return QSize(QFrame::sizeHint().width(), 50);
}

void ServoStateFrame::on_currentValueSlider_rangeChanged(int min, int max)
{
    ui->minCurrentValueLabel->setText(QString::number(min));
    ui->maxCurrentValueLabel->setText(QString::number(max));
}

void ServoStateFrame::on_targetValueSlider_rangeChanged(int min, int max)
{
    ui->minTargetValueLabel->setText(QString::number(min));
    ui->maxTargetValueLabel->setText(QString::number(max));
}

void ServoStateFrame::updateCurrentValue(float fVal)
{
    bool b1 = ui->currentValueSlider->blockSignals(true);
    bool b2 = ui->currentValueEdit->blockSignals(true);

    ui->currentValueSlider->setValue((int)fVal);
    ui->currentValueEdit->setText(QString::number(fVal));

    ui->currentValueSlider->blockSignals(b1);
    ui->currentValueEdit->blockSignals(b2);
}

void ServoStateFrame::updateTargetValue(float fVal)
{
    bool b1 = ui->targetValueSlider->blockSignals(true);
    bool b2 = ui->targetValueSpin->blockSignals(true);

    ui->targetValueSlider->setValue((int)fVal);
    ui->targetValueSpin->setValue(fVal);

    ui->targetValueSlider->blockSignals(b1);
    ui->targetValueSpin->blockSignals(b2);
}

void ServoStateFrame::setTargetValue(float fVal)
{
    if(current_mode == ModeAngle)
    {
        ArmManager::instance()->setServoAngle(servo_id, armio::degToRad(fVal),1);
    }
    else if(current_mode == ModeSpeed)
    {
        ArmManager::instance()->setServoSpeed(servo_id, armio::degToRad(fVal));
    }
    updateTargetValue(fVal);
}

ServoConfig *ServoStateFrame::servoConfig()
{
    return ArmManager::instance()->configAt(servo_id);
}

void ServoStateFrame::updateServoConfig()
{
    ServoConfig* sc = servoConfig();
    if(!sc)
        return;

    ui->idLabel->setText(QString::asprintf("#%d", servo_id));

    ui->currentValueSlider->setRange(armio::radToDeg(sc->minimumAngle()), armio::radToDeg(sc->maximumAngle()));
    if(current_mode == ModeAngle)
    {
        ui->targetValueSlider->setRange(armio::radToDeg(sc->minimumAngle()), armio::radToDeg(sc->maximumAngle()));
        ui->targetValueSpin->setRange(armio::radToDeg(sc->minimumAngle()), armio::radToDeg(sc->maximumAngle()));
    }
    else if(current_mode == ModeSpeed)
    {
        ui->targetValueSlider->setRange(armio::radToDeg(-sc->estimatedSpeed()), armio::radToDeg(sc->estimatedSpeed()));
        ui->targetValueSpin->setRange(armio::radToDeg(-sc->estimatedSpeed()), armio::radToDeg(sc->estimatedSpeed()));
    }
}

void ServoStateFrame::onServoAngleChanged(int id, float fVal)
{
    if(id == servo_id)
    {
        updateCurrentValue(armio::radToDeg(fVal));
    }
}

void ServoStateFrame::on_targetTypeCombo_currentIndexChanged(const QString &arg1)
{
    if(arg1 == "Angle") {
        current_mode = ModeAngle;
    }
    else if(arg1 == "Speed") {
        current_mode = ModeSpeed;
    }
    updateServoConfig();
}

void ServoStateFrame::on_targetValueSlider_sliderMoved(int position)
{
    setTargetValue(position);
}

void ServoStateFrame::on_targetValueSpin_valueChanged(double arg1)
{
    setTargetValue(arg1);
}


#include <QMessageBox>
void ServoStateFrame::on_testBtn_clicked()
{
    ServoConfig* sc = servoConfig();
    if(!sc)
        return;

    float minAngle = sc->minimumAngle();
    float maxAngle = sc->maximumAngle();
    qint64 time_delta = sc->calculateTransitionTimeMs(minAngle, maxAngle, 0) * 2;

    ArmManager::instance()->setServoAngle(servo_id, minAngle, 1);

    while(true) {
        float cur = ArmManager::instance()->servoAngle(servo_id);
        if(fabsf(cur - minAngle) < 0.01)
            break;
        qApp->processEvents();
    }

    QElapsedTimer timer;
    ArmManager::instance()->setServoAngle(servo_id, maxAngle, 1);
    timer.start();

    while(true) {
        float cur = ArmManager::instance()->servoAngle(servo_id);
        if(fabsf(cur - maxAngle) < 0.01)
            break;
        qApp->processEvents();
    }

    ArmManager::instance()->setServoAngle(servo_id, minAngle, 1);

    while(true) {
        float cur = ArmManager::instance()->servoAngle(servo_id);
        if(fabsf(cur - minAngle) < 0.01)
            break;
        qApp->processEvents();
    }

    qint64 elapsed = timer.elapsed();

    QString error_text = QString::asprintf("Estimated time: %d\nMeasured time: %d\nError: %d",(int)time_delta, (int)elapsed, abs(elapsed - time_delta));
    QMessageBox::information(this, "Test result", error_text);
}
