#include "servomapperframe.h"
#include "ui_servomapperframe.h"
#include <QDebug>
#include <QCoreApplication>
#include <QJsonObject>

ServoMapperFrame::ServoMapperFrame(QMainWindow* mw, QWidget *parent) :
    QFrame(parent),
    ui(new Ui::ServoMapperFrame)
{
    ui->setupUi(this);


    m_shortcut = new QShortcut(mw);
    m_shortcut->setAutoRepeat(true);
    m_shortcut->setContext(Qt::ShortcutContext::ApplicationShortcut);
    connect(m_shortcut, &QShortcut::activated, this, &ServoMapperFrame::onShortcutActivated);

    connect(ui->shortcutGrabber, &ShortcutGrabber::keySequenceChanged, this, &ServoMapperFrame::onShortcutKeySequenceChanged);
}

ServoMapperFrame::~ServoMapperFrame()
{
    if(m_shortcut) {
        delete m_shortcut;
        m_shortcut = nullptr;
    }
    delete ui;
}

bool ServoMapperFrame::saveMapping(QJsonObject &obj) const
{
    obj["mapper_type"] = (int)m_mapperType;
    obj["expression"] = ui->expressionEdit->text();

    switch(m_mapperType) {
    case kDirectInputAxis:
        break;
    case kDirectInputButton:
        break;
    case kKeyboardShortcut:
        obj["shortcut"] = m_shortcut->key().toString();
        break;
    }
    return true;
}

bool ServoMapperFrame::loadMapping(const QJsonObject &obj)
{
    MapperType mtype = (MapperType)obj["mapper_type"].toInt();
    if((mtype < kUnknown) || (mtype >= kLast))
        return false;

    setMapperType(mtype);
    ui->expressionEdit->setText(obj["expression"].toString());

    switch(m_mapperType) {
    case kDirectInputAxis:
        break;
    case kDirectInputButton:
        break;
    case kKeyboardShortcut:
        ui->shortcutGrabber->setKeySequence(QKeySequence::fromString(obj["shortcut"].toString()));
        break;
    }
    return true;
}

void ServoMapperFrame::setMapperType(ServoMapperFrame::MapperType type)
{
    switch(type) {
    case kDirectInputAxis:
        ui->mapperTypeLabel->setText("DI Axis");
        ui->stackedWidget->setCurrentWidget(ui->dinputPage);

        ui->dinputControlCombo->clear();
        for(int i = 0; i < m_dinputCtrl->axisCount(); i++)
            ui->dinputControlCombo->addItem(m_dinputCtrl->axisName(i), QVariant::fromValue(i));
        if(m_dinputCtrl->axisCount())
            ui->dinputControlCombo->setCurrentIndex(0);
        break;
    case kDirectInputButton:
        ui->mapperTypeLabel->setText("DI Button");
        ui->stackedWidget->setCurrentWidget(ui->dinputPage);

        ui->dinputControlCombo->clear();
        for(int i = 0; i < m_dinputCtrl->buttonCount(); i++)
            ui->dinputControlCombo->addItem(m_dinputCtrl->buttonName(i), QVariant::fromValue(i));
        if(m_dinputCtrl->axisCount())
            ui->dinputControlCombo->setCurrentIndex(0);
        break;
    case kKeyboardShortcut:
        ui->mapperTypeLabel->setText("Shortcut");
        ui->stackedWidget->setCurrentWidget(ui->shortcutPage);
        break;
    }

    m_mapperType = type;
}

QJSEngine *ServoMapperFrame::scriptEngine() const
{
    return m_scriptEngine;
}

void ServoMapperFrame::setScriptEngine(QJSEngine *scriptEngine)
{
    m_scriptEngine = scriptEngine;
}

DInputCtrl *ServoMapperFrame::dinputCtrl() const
{
    return m_dinputCtrl;
}

void ServoMapperFrame::setDinputCtrl(DInputCtrl *dinputCtrl)
{
    if(m_dinputCtrl) {
        disconnect(m_dinputCtrl, 0, this, 0);
    }

    m_dinputCtrl = dinputCtrl;

    if(m_dinputCtrl) {
        connect(m_dinputCtrl, &DInputCtrl::axisValueChanged, this, &ServoMapperFrame::onDIAxisValueChanged);
        connect(m_dinputCtrl, &DInputCtrl::buttonPressed, this, &ServoMapperFrame::onDIButtonPressed);
    }
}

void ServoMapperFrame::onShortcutKeySequenceChanged(QKeySequence ks)
{
    m_shortcut->setKey(ks);
    m_shortcut->setEnabled(true);
}

void ServoMapperFrame::onShortcutActivated()
{
    setCurrentValue(1.0);
}

void ServoMapperFrame::setCurrentValue(double val)
{
    if(!m_scriptEngine)
        return;

    ui->currentValueEdit->setText(QString::number(val));

    QString CodeFunction = ui->expressionEdit->text();
    QJSValue fun = m_scriptEngine->evaluate("(function fun(x) {" + CodeFunction+";})");

    QJSValueList args;
    args << val;
//    QJSValue value = fun.call(args);
    fun.call(args);
    //    qDebug() << "Eval value: " << value.toNumber();
}

void ServoMapperFrame::onDIButtonPressed(GUID controllerId, uint btnId, bool bPressed)
{
    if((m_mapperType == kDirectInputButton) && (btnId == m_diCurrId)) {
        setCurrentValue(bPressed ? 1.0 : 0.0);
    }
}

void ServoMapperFrame::onDIAxisValueChanged(GUID controllerId, uint axisId, float value)
{
    if((m_mapperType == kDirectInputAxis) && (axisId == m_diCurrId)) {
        setCurrentValue(value);
    }
}


void ServoMapperFrame::on_dinputControlCombo_currentIndexChanged(int index)
{
    if((m_mapperType == kDirectInputButton) || (m_mapperType == kDirectInputAxis))
        m_diCurrId = ui->dinputControlCombo->itemData(index).toInt();
}
