#include "servoconfigeditdialog.h"
#include "ui_servoconfigeditdialog.h"

ServoConfigEditDialog::ServoConfigEditDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ServoConfigEditDialog)
{
    ui->setupUi(this);
}

ServoConfigEditDialog::~ServoConfigEditDialog()
{
    delete ui;
}

ServoConfig *ServoConfigEditDialog::config() const
{
    return m_config;
}

void ServoConfigEditDialog::setConfig(ServoConfig *config)
{
    m_config = config;

    ui->idSpin->setValue(m_config->servoId());
    ui->estimatedSpeedSpin->setValue(armio::radToDeg(m_config->estimatedSpeed()));
    ui->maxAngleSpin->setValue(armio::radToDeg(m_config->maximumAngle()));
    ui->minAngleSpin->setValue(armio::radToDeg(m_config->minimumAngle()));
    ui->maxPulseWeightSpin->setValue(m_config->maxPulseWeight());
    ui->minPulseWeightSpin->setValue(m_config->minPulseWeight());
    ui->pulseFreqSpin->setValue(m_config->pulseFrequency());
}

void ServoConfigEditDialog::disableId(bool bVal)
{
    ui->idSpin->setDisabled(bVal);
}

void ServoConfigEditDialog::on_buttonBox_accepted()
{
    if(!m_config)
        return;

    m_config->setServoId(ui->idSpin->value());
    m_config->setEstimatedSpeed(armio::degToRad(ui->estimatedSpeedSpin->value()));
    m_config->setMaximumAngle(armio::degToRad(ui->maxAngleSpin->value()));
    m_config->setMinimumAngle(armio::degToRad(ui->minAngleSpin->value()));
    m_config->setMaxPulseWeight(ui->maxPulseWeightSpin->value());
    m_config->setMinPulseWeight(ui->minPulseWeightSpin->value());
    m_config->setPulseFrequency(ui->pulseFreqSpin->value());
}
