#ifndef SERVOMAPPERFRAME_H
#define SERVOMAPPERFRAME_H

#include <QFrame>
#include <QJSEngine>
#include <QShortcut>
#include <QMainWindow>
#include "../dinput/dinputctrl.h"

namespace Ui {
class ServoMapperFrame;
}

class ServoMapperFrame : public QFrame
{
    Q_OBJECT
public:
    enum MapperType {
        kUnknown = 0,
        kDirectInputAxis,
        kDirectInputButton,
        kKeyboardShortcut,
        kLast
    };
public:
    explicit ServoMapperFrame(QMainWindow* mw, QWidget *parent = 0);
    ~ServoMapperFrame();

    bool saveMapping(QJsonObject& obj) const;
    bool loadMapping(const QJsonObject& obj);

    void setMapperType(MapperType type);

    QJSEngine *scriptEngine() const;
    void setScriptEngine(QJSEngine *scriptEngine);

    DInputCtrl *dinputCtrl() const;
    void setDinputCtrl(DInputCtrl *dinputCtrl);

protected slots:
    void onShortcutKeySequenceChanged(QKeySequence ks);
    void onShortcutActivated();
    void setCurrentValue(double val);
    void onDIButtonPressed(GUID controllerId, uint btnId, bool bPressed);
    void onDIAxisValueChanged(GUID controllerId, uint axisId, float value);
private slots:
    void on_dinputControlCombo_currentIndexChanged(int index);

private:
    Ui::ServoMapperFrame *ui;
    MapperType m_mapperType = kUnknown;
    QJSEngine* m_scriptEngine = nullptr;
    DInputCtrl* m_dinputCtrl = nullptr;
    uint m_diCurrId = -1;

    QShortcut* m_shortcut = nullptr;
};

#endif // SERVOMAPPERFRAME_H
