#ifndef SHORTCUTGRABBER_H
#define SHORTCUTGRABBER_H

#include <QLineEdit>
#include <QKeySequence>

class ShortcutGrabber : public QLineEdit
{
    Q_OBJECT
public:
    explicit ShortcutGrabber(QWidget *parent = 0);

    void keyPressEvent(QKeyEvent *event);

    QKeySequence keySequence() const;
    void setKeySequence(const QKeySequence &keySequence);
signals:
    void keySequenceChanged(QKeySequence keySequence);
private:
    QKeySequence m_keySequence;
};

#endif // SHORTCUTGRABBER_H
