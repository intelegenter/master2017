#ifndef SERVOCONFIGEDITDIALOG_H
#define SERVOCONFIGEDITDIALOG_H

#include <QDialog>
#include "../armservo.h"

namespace Ui {
class ServoConfigEditDialog;
}

class ServoConfigEditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ServoConfigEditDialog(QWidget *parent = 0);
    ~ServoConfigEditDialog();

    ServoConfig *config() const;
    void setConfig(ServoConfig *config);

    void disableId(bool bVal);

private slots:
    void on_buttonBox_accepted();

private:
    Ui::ServoConfigEditDialog *ui;
    ServoConfig* m_config = nullptr;
};

#endif // SERVOCONFIGEDITDIALOG_H
