#ifndef EASINGCURVEWIDGET_H
#define EASINGCURVEWIDGET_H

#include <QWidget>
#include <QEasingCurve>
#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QGraphicsPathItem>


namespace Ui {
class EasingCurveWidget;
}

class EasingCurveWidget : public QWidget
{
    Q_OBJECT

public:
    explicit EasingCurveWidget(QWidget *parent = 0);
    ~EasingCurveWidget();

    QEasingCurve curve() const;
    void setCurve(const QEasingCurve &curve);

    void showEvent( QShowEvent* event );
    void resizeEvent(QResizeEvent* event);
signals:
    void curveChanged();
public slots:
    void updateCurve();
    void redraw();
private slots:
    void on_typeCombo_currentIndexChanged(int index);

    void on_amplitudeSpin_valueChanged(double arg1);

    void on_overshootSpin_valueChanged(double arg1);

    void on_periodSpin_valueChanged(double arg1);

private:
    Ui::EasingCurveWidget *ui;
    QEasingCurve m_easingCurve;
    bool b_disable_update_signal = false;

    QGraphicsScene* m_scene = nullptr;
    QGraphicsTextItem* m_legend = nullptr;
    QGraphicsPathItem* m_curve = nullptr;
};

#endif // EASINGCURVEWIDGET_H
