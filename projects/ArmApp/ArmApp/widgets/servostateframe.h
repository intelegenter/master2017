#ifndef SERVOSTATEFRAME_H
#define SERVOSTATEFRAME_H

#include <QFrame>
#include "../armservo.h"
#include "../armmanager.h"

namespace Ui {
class ServoStateFrame;
}

class ServoStateFrame : public QFrame
{
    Q_OBJECT
    enum eMode {
        ModeAngle,
        ModeSpeed
    };

public:
    explicit ServoStateFrame(QWidget *parent = 0);
    ~ServoStateFrame();

    void setServoId(int val);
    QSize sizeHint() const;
public slots:
    void updateServoConfig();
private slots:
    void onServoAngleChanged(int id, float fVal);

private slots:
    void on_currentValueSlider_rangeChanged(int min, int max);

    void on_targetValueSlider_rangeChanged(int min, int max);

    void on_targetTypeCombo_currentIndexChanged(const QString &arg1);

    void on_targetValueSlider_sliderMoved(int position);

    void on_targetValueSpin_valueChanged(double arg1);

    void on_testBtn_clicked();

private:
    void updateCurrentValue(float fVal);
    void updateTargetValue(float fVal);
    void setTargetValue(float fVal);
    ServoConfig* servoConfig();
private:
    Ui::ServoStateFrame *ui;
    int servo_id = -1;
    eMode current_mode = ModeAngle;
};

#endif // SERVOSTATEFRAME_H
