#include "easingcurvewidget.h"
#include "ui_easingcurvewidget.h"
#include <QMetaEnum>
#include <QDebug>


EasingCurveWidget::EasingCurveWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EasingCurveWidget)
{
    ui->setupUi(this);

    QMetaEnum metaEnum = QMetaEnum::fromType<QEasingCurve::Type>();
    for(int i = 0; i < metaEnum.keyCount(); i++) {
        ui->typeCombo->addItem(metaEnum.key(i), metaEnum.value(i));
    }

    m_scene = new QGraphicsScene(ui->graphicsView);
    ui->graphicsView->setScene(m_scene);

    m_legend = new QGraphicsTextItem(QString("None"));
//    m_legend->setTe
    m_scene->addItem(m_legend);

    m_curve = new QGraphicsPathItem();
    m_scene->addItem(m_curve);

    m_curve->setPen(QPen(QColor(Qt::blue), 0.01));

    connect(this, &EasingCurveWidget::curveChanged, this, &EasingCurveWidget::redraw);
    updateCurve();
}

EasingCurveWidget::~EasingCurveWidget()
{
    delete ui;
}

QEasingCurve EasingCurveWidget::curve() const
{
    return m_easingCurve;
}

void EasingCurveWidget::setCurve(const QEasingCurve &curve)
{
    m_easingCurve = curve;
    updateCurve();
    emit curveChanged();
}

void EasingCurveWidget::updateCurve()
{
    b_disable_update_signal = true;
    for(int i = 0; i < ui->typeCombo->count(); i++)
        if(ui->typeCombo->itemData(i).toInt() == (int)m_easingCurve.type())
            ui->typeCombo->setCurrentIndex(i);
    ui->amplitudeSpin->setValue(m_easingCurve.amplitude());
    ui->periodSpin->setValue(m_easingCurve.period());
    ui->overshootSpin->setValue(m_easingCurve.overshoot());
    b_disable_update_signal = false;
}

void EasingCurveWidget::on_typeCombo_currentIndexChanged(int index)
{
    if(b_disable_update_signal) return;
    m_easingCurve.setType((QEasingCurve::Type)ui->typeCombo->itemData(index).toInt());
    emit curveChanged();
}

void EasingCurveWidget::on_amplitudeSpin_valueChanged(double arg1)
{
    if(b_disable_update_signal) return;
    m_easingCurve.setAmplitude(arg1);
    emit curveChanged();
}

void EasingCurveWidget::on_overshootSpin_valueChanged(double arg1)
{
    if(b_disable_update_signal) return;
    m_easingCurve.setOvershoot(arg1);
    emit curveChanged();
}

void EasingCurveWidget::on_periodSpin_valueChanged(double arg1)
{
    if(b_disable_update_signal) return;
    m_easingCurve.setPeriod(arg1);
    emit curveChanged();
}

void EasingCurveWidget::showEvent( QShowEvent* event ) {
    QWidget::showEvent( event );
    redraw();
}

void EasingCurveWidget::resizeEvent(QResizeEvent* event)
{
   QWidget::resizeEvent(event);
   redraw();
}

void EasingCurveWidget::redraw()
{
//    qDebug() << "Redraw";
    QPainterPath path;

    const float delta = 0.05;
    path.moveTo(0, m_easingCurve.valueForProgress(0));
    for(float c = delta; c <= 1.f; c += delta) {
        path.lineTo(c, m_easingCurve.valueForProgress(c));
    }
    m_curve->setPath(path);
    QRectF chartBBox = m_curve->boundingRect();
    QSizeF growSize(chartBBox.width() / 20.f, chartBBox.height() / 20.f);
    QRectF chartBBoxGrowed = chartBBox.marginsAdded(QMarginsF(growSize.width(),growSize.height(),growSize.width(),growSize.height()));

    QRectF visibleRect = ui->graphicsView->viewport()->geometry();

    QTransform tr;
    tr.scale(visibleRect.width() / chartBBoxGrowed.width(),
             (visibleRect.height() / chartBBoxGrowed.height()));
    m_curve->setTransform(tr);

    ui->graphicsView->fitInView(visibleRect,Qt::IgnoreAspectRatio);

//    QRectF visibleRect = ui->graphicsView->mapToScene(ui->graphicsView->viewport()->geometry()).boundingRect();

//    QTransform tr;
//    tr.scale(visibleRect.width() / chartBBoxGrowed.width(),
//             -(visibleRect.height() / chartBBoxGrowed.height()));
//    QPointF trP = chartBBoxGrowed.topLeft() - visibleRect.topLeft();
//    tr.translate(trP.x(), trP.y());
//    m_curve->setTransform(tr);

    m_legend->setPlainText(QString::asprintf("x=%.1f..%.1f\ny=%.1f..%.1f", chartBBox.left(),chartBBox.right(), chartBBox.bottom(), chartBBox.top()));




//    ui->graphicsView->sce

//    ui->graphicsView->fitInView(m_curve->boundingRect(),Qt::IgnoreAspectRatio);
    ui->graphicsView->update();
}
