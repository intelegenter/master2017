#ifndef SERVOCOMMANDFRAME_H
#define SERVOCOMMANDFRAME_H

#include <QFrame>

namespace Ui {
class ServoCommandFrame;
}

class ServoCommandFrame : public QFrame
{
    Q_OBJECT

public:
    explicit ServoCommandFrame(QWidget *parent = 0);
    ~ServoCommandFrame();

private:
    Ui::ServoCommandFrame *ui;
};

#endif // SERVOCOMMANDFRAME_H
