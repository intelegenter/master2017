#include "shortcutgrabber.h"
#include <QKeyEvent>
#include <QDebug>
ShortcutGrabber::ShortcutGrabber(QWidget *parent) : QLineEdit(parent)
{

}

void ShortcutGrabber::keyPressEvent(QKeyEvent *event)
{
    if((event->key() == Qt::Key_unknown) || (event->key() == 0))
        return;

    if((event->key() == Qt::Key_Shift) || (event->key() == Qt::Key_Control) ||
       (event->key() == Qt::Key_Meta)  || (event->key() == Qt::Key_Alt))
        return;

    QKeySequence ks(event->modifiers() | event->key());
    setKeySequence(ks);
}

QKeySequence ShortcutGrabber::keySequence() const
{
    return m_keySequence;
}

void ShortcutGrabber::setKeySequence(const QKeySequence &keySequence)
{
    if(m_keySequence != keySequence)
    {
        m_keySequence = keySequence;
        setText(m_keySequence.toString());
        emit keySequenceChanged(m_keySequence);
    }
}

