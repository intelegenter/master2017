#include "servocommandframe.h"
#include "ui_servocommandframe.h"

ServoCommandFrame::ServoCommandFrame(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::ServoCommandFrame)
{
    ui->setupUi(this);
}

ServoCommandFrame::~ServoCommandFrame()
{
    delete ui;
}
