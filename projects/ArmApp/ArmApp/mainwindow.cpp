#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QMessageBox>
#include <QJsonDocument>
#include <QFileDialog>
#include <QListWidgetItem>
#include <QDebug>
#include <QQmlEngine>
#include <QJsonArray>
#include "widgets/servoconfigeditdialog.h"
#include "widgets/servostateframe.h"
#include "widgets/servomapperframe.h"
#include <QPropertyAnimation>
#include <QMetaEnum>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_settings = new QSettings("config.cfg", QSettings::IniFormat, this);

    qRegisterMetaType<ArmMessage::Pointer>("ArmMessage::Pointer");
    m_connection = new ArmConnection(this);
    connect(m_connection, &ArmConnection::messageReceived,
            this, &MainWindow::onMessageReceived);
    connect(m_connection, &ArmConnection::connectionLost,
            this, &MainWindow::onConnectionLost);
    connect(m_connection, &ArmConnection::connectionEstablished,
            this, &MainWindow::onConnectionEstablished);
    connect(m_connection, &ArmConnection::connectionError,
            this, &MainWindow::onConnectionError);
    connect(m_connection, &ArmConnection::kbytesRW,
            this, &MainWindow::onSerialIO);

    ArmManager::instance()->setConnection(m_connection);
//    connect(ArmManager::instance(), &ArmManager::transitionStarted, this, &MainWindow::onTransitionStarted);
//    connect(ArmManager::instance(), &ArmManager::transitionFinished, this, &MainWindow::onTransitionFinished);
    connect(ArmManager::instance(), &ArmManager::servoAngleChanged, this, &MainWindow::onServoAngleChanged);
//    connect(ArmManager::instance(), &ArmManager::servoTransitionAngleChanged, this, &MainWindow::onServoTransitionAngleChanged);
//    connect(ArmManager::instance(), &ArmManager::servoTransitionStarted, this, &MainWindow::onServoTransitionStarted);
//    connect(ArmManager::instance(), &ArmManager::servoTransitionFinished, this, &MainWindow::onServoTransitionFinished);
    connect(ArmManager::instance(), &ArmManager::configChanged, this, &MainWindow::onConfigChanged);
    connect(ArmManager::instance()->currentSequence(), &ServoSequence::statesChanged,
            this, &MainWindow::updateStateTable);
    connect(ArmManager::instance()->currentSequence(), &ServoSequence::animationsChanged,
            this, &MainWindow::updateTransitionsTable);
    connect(ArmManager::instance()->currentSequence(), &ServoSequence::stateChanged,
            this, &MainWindow::sequenceStateChanged);
    connect(ui->easingCurveWidget, &EasingCurveWidget::curveChanged,
            this, &MainWindow::onEasingCurveChanged);


    m_dinputCtrl = new DInputCtrl(this);

    update_servo_table_timer = new QTimer(this);
    update_servo_table_timer->setSingleShot(true);
    update_servo_table_timer->setInterval(30);
    connect(update_servo_table_timer, &QTimer::timeout,
            this, &MainWindow::updateServoTable);

    updateSerialPortData();
    ui->editServoWidget->setVisible(false);

    m_scriptEngine = new QJSEngine(this);
    QJSValue armManagerObject = m_scriptEngine->newQObject( ArmManager::instance() );
    m_scriptEngine->globalObject().setProperty("ArmManager", armManagerObject);
    QQmlEngine::setObjectOwnership(ArmManager::instance(), QQmlEngine::CppOwnership);

    ui->splitter->setStretchFactor(0,3);
    ui->splitter->setStretchFactor(1,7);

    loadSettings();
}

MainWindow::~MainWindow()
{
    qDebug() << "MainWindow::~MainWindow()";
    delete ui;
}

void MainWindow::loadSettings()
{
    QString configPath = m_settings->value("CurrentConfigPath", QVariant::fromValue(QString())).toString();
    QString mappingPath = m_settings->value("CurrentMappingPath", QVariant::fromValue(QString())).toString();
    QString sequencePath = m_settings->value("CurrentServoSequencePath", QVariant::fromValue(QString())).toString();

    bool bOk = false;
    float tmul = m_settings->value("tmul").toFloat(&bOk);
    ui->transitionMultSpin->setValue(bOk ? tmul : ServoConfig::TransitionMult);

    float tadd = m_settings->value("tadd").toFloat(&bOk);
    ui->transitionAddSpin->setValue(bOk ? tadd : ServoConfig::AdditionalTime);

    if(!configPath.isEmpty())
        ArmManager::instance()->loadConfig(configPath);
    if(!mappingPath.isEmpty())
        loadServoMapping(mappingPath);
    if(!sequencePath.isEmpty())
        ArmManager::instance()->currentSequence()->load(sequencePath);
}

int MainWindow::getTableCurrentServoId() const
{
    int currColIndex = ui->servoTable->currentColumn();
    if(currColIndex == -1)
        return -1;

    QTableWidgetItem* item = ui->servoTable->item(0,currColIndex);
    if(item == nullptr)
        return -1;

    bool bOk = false;
    int val = item->text().toInt(&bOk);
    if(!bOk)
        return -1;
    return val;
}

void MainWindow::updateSerialPortData()
{
    ui->portNameCombo->clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        ui->portNameCombo->addItem(info.portName());
    }

    ui->baudRateCombo->clear();
    ui->baudRateCombo->addItem("1200", QVariant::fromValue(QSerialPort::Baud1200));
    ui->baudRateCombo->addItem("2400", QVariant::fromValue(QSerialPort::Baud2400));
    ui->baudRateCombo->addItem("4800", QVariant::fromValue(QSerialPort::Baud4800));
    ui->baudRateCombo->addItem("9600", QVariant::fromValue(QSerialPort::Baud9600));
    ui->baudRateCombo->addItem("19200", QVariant::fromValue(QSerialPort::Baud19200));
    ui->baudRateCombo->addItem("38400", QVariant::fromValue(QSerialPort::Baud38400));
    ui->baudRateCombo->addItem("57600", QVariant::fromValue(QSerialPort::Baud57600));
    ui->baudRateCombo->addItem("115200", QVariant::fromValue(QSerialPort::Baud115200));

    ui->dataBitsCombo->clear();
    ui->dataBitsCombo->addItem("5 bit", QVariant::fromValue(QSerialPort::Data5));
    ui->dataBitsCombo->addItem("6 bit", QVariant::fromValue(QSerialPort::Data6));
    ui->dataBitsCombo->addItem("7 bit", QVariant::fromValue(QSerialPort::Data7));
    ui->dataBitsCombo->addItem("8 bit", QVariant::fromValue(QSerialPort::Data8));

    ui->parityBitsCombo->clear();
    ui->parityBitsCombo->addItem("None", QVariant::fromValue(QSerialPort::NoParity));
    ui->parityBitsCombo->addItem("Even", QVariant::fromValue(QSerialPort::EvenParity));
    ui->parityBitsCombo->addItem("Odd", QVariant::fromValue(QSerialPort::OddParity));
    ui->parityBitsCombo->addItem("Space", QVariant::fromValue(QSerialPort::SpaceParity));
    ui->parityBitsCombo->addItem("Mark", QVariant::fromValue(QSerialPort::MarkParity));

    ui->stopBitsCombo->clear();
    ui->stopBitsCombo->addItem("1 stop bit", QVariant::fromValue(QSerialPort::OneStop));
    ui->stopBitsCombo->addItem("1.5 stop bit", QVariant::fromValue(QSerialPort::OneAndHalfStop));
    ui->stopBitsCombo->addItem("2 stop bits", QVariant::fromValue(QSerialPort::TwoStop));

    ui->armMessageSelectorCombo->clear();
    ui->armMessageSelectorCombo->addItem("kArmMsgData_ServoDecl",   QVariant::fromValue((int)kArmMsgData_ServoDecl));
    ui->armMessageSelectorCombo->addItem("kArmMsgData_ServoState",  QVariant::fromValue((int)kArmMsgData_ServoState));
    ui->armMessageSelectorCombo->addItem("kArmMsgData_DebugInfo",   QVariant::fromValue((int)kArmMsgData_DebugInfo));
    ui->armMessageSelectorCombo->addItem("kArmMsgData_Service",     QVariant::fromValue((int)kArmMsgData_Service));
    ui->armMessageSelectorCombo->addItem("kArmMsgData_Echo",        QVariant::fromValue((int)kArmMsgData_Echo));


    //Loading last saved setup
    ui->portNameCombo->setCurrentText(m_settings->value("SerialPort/PortName").toString());
    QSerialPort::BaudRate baudRate =
            (QSerialPort::BaudRate)m_settings->value("SerialPort/BaudRate", QVariant::fromValue(QSerialPort::Baud115200)).toInt();
    QSerialPort::DataBits dataBits =
            (QSerialPort::DataBits)m_settings->value("SerialPort/DataBits", QVariant::fromValue(QSerialPort::Data8)).toInt();
    QSerialPort::Parity parity =
            (QSerialPort::Parity)m_settings->value("SerialPort/Parity", QVariant::fromValue(QSerialPort::NoParity)).toInt();
    QSerialPort::StopBits stopBits =
            (QSerialPort::StopBits)m_settings->value("SerialPort/StopBits", QVariant::fromValue(QSerialPort::OneStop)).toInt();

    for(int i = 0; i < ui->baudRateCombo->count(); i++)
        if(ui->baudRateCombo->itemData(i) == QVariant::fromValue(baudRate))
            ui->baudRateCombo->setCurrentIndex(i);
    for(int i = 0; i < ui->dataBitsCombo->count(); i++)
        if(ui->dataBitsCombo->itemData(i) == QVariant::fromValue(dataBits))
            ui->dataBitsCombo->setCurrentIndex(i);
    for(int i = 0; i < ui->parityBitsCombo->count(); i++)
        if(ui->parityBitsCombo->itemData(i) == QVariant::fromValue(parity))
            ui->parityBitsCombo->setCurrentIndex(i);
    for(int i = 0; i < ui->stopBitsCombo->count(); i++)
        if(ui->stopBitsCombo->itemData(i) == QVariant::fromValue(stopBits))
            ui->stopBitsCombo->setCurrentIndex(i);

    ui->joystickCombo->clear();
    m_joysticksList = DInputCtrl::EnumJoysticks();

    ui->joystickCombo->addItem("No joystick", QVariant::fromValue(-1));
    for(int i = 0; i < m_joysticksList.size(); i++)
        ui->joystickCombo->addItem(m_joysticksList.at(i).name,QVariant::fromValue(i));

    switchToJoystick(m_settings->value("LastJoystick").toString());

    //    QList<DInputCtrl::JoystickDecl> list = DInputCtrl::EnumJoysticks();
    //    if(!list.isEmpty()) {

    //        m_dinputCtrl->create(list.first().id);
    //    }
    //    Q_ASSERT(m_dinputCtrl);
}

void MainWindow::resetConnection()
{
    m_connection->stop();

    bool bOk = false;
    QString portName = ui->portNameCombo->currentText();
    if(portName.isEmpty()) {
        QMessageBox::warning(this,"Warning", "Port name is empty!");
        return;
    }

    QSerialPort::BaudRate baudRate = ui->baudRateCombo->currentData().value<QSerialPort::BaudRate>();
    QSerialPort::DataBits dataBits = ui->dataBitsCombo->currentData().value<QSerialPort::DataBits>();
    QSerialPort::Parity parity = ui->parityBitsCombo->currentData().value<QSerialPort::Parity>();
    QSerialPort::StopBits stopBits = ui->stopBitsCombo->currentData().value<QSerialPort::StopBits>();

    m_settings->setValue("SerialPort/PortName", QVariant::fromValue(portName));
    m_settings->setValue("SerialPort/BaudRate", QVariant::fromValue((int)baudRate));
    m_settings->setValue("SerialPort/DataBits", QVariant::fromValue((int)dataBits));
    m_settings->setValue("SerialPort/Parity",   QVariant::fromValue((int)parity));
    m_settings->setValue("SerialPort/StopBits", QVariant::fromValue((int)stopBits));

    m_connection->setPortName(portName);
    m_connection->setBaudRate(baudRate);
    m_connection->setDataBits(dataBits);
    m_connection->setParity(parity);
    m_connection->setStopBits(stopBits);
    m_connection->start();
}

void MainWindow::onConnectionEstablished()
{
    qDebug() << "-------------Connection established!";
    ui->connectBtn->setText("Disconnect");
}

void MainWindow::onConnectionLost()
{
    qDebug() << "-------------Connection lost!";
    ui->connectBtn->setText("Connect");
}

void MainWindow::onConnectionError(QSerialPort::SerialPortError errcode, QString errorText)
{
    QMessageBox::warning(this, "Connection error!", QString::asprintf("Code=%d, ", errcode) + errorText);
}

void MainWindow::updateServoTable()
{
    ArmManager* mgr = ArmManager::instance();
    int servoCount = 0;
    for(int i = 0; i < mgr->servoCount(); i++) {
        if(mgr->configAt(i))
            servoCount++;
    }

    ui->servoTable->setColumnCount(servoCount);

    int col_idx = 0;
    for(int i = 0; i < mgr->servoCount(); i++)
    {
        ServoConfig* config = mgr->configAt(i);
        if(!config)
            continue;

        for(int j = 0; j < ui->servoTable->rowCount(); j++)
            loadItemToServoTable(j, col_idx, i);
        col_idx++;
    }
}

void MainWindow::updateServoTableLater()
{
    if(!update_servo_table_timer->isActive())
        update_servo_table_timer->start();
}

void MainWindow::loadItemToServoTable(int row, int col, int servoId)
{
    QTableWidgetItem* item = ui->servoTable->item(row, col);
    if(!item) {
        item = new QTableWidgetItem("");
        ui->servoTable->setItem(row, col, item);
    }

    ArmManager* mgr = ArmManager::instance();
    ServoConfig* servoConf = mgr->configAt(servoId);
    if(!servoConf) {
        Q_ASSERT(false);
        return;
    }

    switch(row) {
    case 0: //id
        item->setText(QString::number(servoId));
        break;
    case 1: //Current angle
        item->setText(QString::number(armio::radToDeg(mgr->servoAngle(servoId))));
        break;
    case 2: //Next angle
        item->setText(QString::number(armio::radToDeg(0)));
        break;
    case 3: //Speed
        item->setText(QString::number(armio::radToDeg(mgr->servoSpeed(servoId))));
        break;
    case 4: //Transition state
        item->setText(QString::number(0));
        break;
    case 5: //Min pulse weight
        item->setText(QString::number(servoConf->minPulseWeight()));
        break;
    case 6: //Max pulse weight
        item->setText(QString::number(servoConf->maxPulseWeight()));
        break;
    case 7: //Pulse freq
        item->setText(QString::number(servoConf->pulseFrequency()));
        break;
    case 8: //Estimated speed
        item->setText(QString::number(armio::radToDeg(servoConf->estimatedSpeed())));
        break;
    case 9: //Min angle
        item->setText(QString::number(armio::radToDeg(servoConf->minimumAngle())));
        break;
    case 10: //Max angle
        item->setText(QString::number(armio::radToDeg(servoConf->maximumAngle())));
        break;
    }
}

void MainWindow::onSerialIO(float wr, float rd)
{
    statusBar()->showMessage(QString::asprintf("R: %.1f KB/s W: %.1f KB/s", rd, wr));
}

//void MainWindow::onTransitionStarted()
//{
//    updateServoTableLater();
//}

//void MainWindow::onTransitionFinished()
//{
//    updateServoTableLater();
//}

void MainWindow::onServoAngleChanged(int id, float angle)
{
    updateServoTableLater();
}

//void MainWindow::onServoTransitionAngleChanged(int id, float angle)
//{
//    updateServoTableLater();
//}

//void MainWindow::onServoTransitionStarted(int id)
//{
//    updateServoTableLater();
//}

//void MainWindow::onServoTransitionFinished(int id)
//{
//    updateServoTableLater();
//}

void MainWindow::onConfigChanged()
{
    updateServoTableLater();

    ui->servoCtlList->clear();
    for(int i = 0; i < ArmManager::instance()->servoCount(); i++)
    {
        ServoConfig* sc = ArmManager::instance()->configAt(i);
        if(!sc)
            continue;

        QListWidgetItem* item = new QListWidgetItem(ui->servoCtlList);
        ServoStateFrame* frame = new ServoStateFrame();
        item->setSizeHint(frame->sizeHint());
        frame->setServoId(i);

        ui->servoCtlList->setItemWidget(item,frame);
    }
}

void MainWindow::on_connectBtn_clicked()
{
    if(m_connection->isConnected())
        m_connection->stop();
    else
        resetConnection();
}

void MainWindow::onMessageReceived(ArmMessage::Pointer message)
{
    QJsonDocument doc;
    QJsonObject obj;
    if(!message->toJson(obj)) {
        ui->recvMsgBrowser->append("Unknown message type!\n");
        return;
    }
    doc.setObject(obj);

    ui->recvMsgBrowser->append(doc.toJson(QJsonDocument::Indented));
}


void MainWindow::on_sendRawMessageBtn_clicked()
{
    if(!m_connection)
        return;

    QJsonDocument doc = QJsonDocument::fromJson(ui->rawMessageTextEdit->toPlainText().toUtf8());
    if(doc.isNull() || doc.isEmpty() || !doc.isObject()) {
        QMessageBox::warning(this, "Error!", "Failed to parse JSon code!");
        return;
    }

    ArmMessage::Pointer msg(new ArmMessage());
    if(!msg->createFromJson(doc.object())) {
        QMessageBox::warning(this, "Error!", "Failed to create ArmMessage!");
        return;
    }

//    QMetaObject::invokeMethod( m_connection, "sendMessage", Q_ARG( ArmMessage::Pointer, msg ) );
    m_connection->sendMessage(msg);
}

void MainWindow::on_armMessageSelectorCombo_currentIndexChanged(int index)
{
    ui->rawMessageTextEdit->clear();
    ArmMessage::Pointer msg = ArmMessage::Create((ArmMsgDataType)ui->armMessageSelectorCombo->currentData().toInt());

    QJsonDocument doc;
    QJsonObject obj;
    if(!msg->toJson(obj)) {
        QMessageBox::warning(this, "Error!", "Failed to create message!");
        return;
    }

    doc.setObject(obj);
    ui->rawMessageTextEdit->setPlainText(doc.toJson(QJsonDocument::Indented));
}

void MainWindow::on_addServoBtn_clicked()
{
    ServoConfig conf;
    ServoConfigEditDialog dial;
    dial.setModal(true);
    dial.setConfig(&conf);
    if(dial.exec() == QDialog::Accepted)
    {
        ArmManager::instance()->setConfig(conf);
    }
}

void MainWindow::on_editCurrentServo_clicked()
{
    int id = getTableCurrentServoId();
    if(id == -1)
        return;

    ServoConfig* pConf = ArmManager::instance()->configAt(id);
    if(!pConf)
        return;

    ServoConfig conf = *pConf;
    ServoConfigEditDialog dial;
    dial.setModal(true);
    dial.setConfig(&conf);
    dial.disableId(true);
    if(dial.exec() == QDialog::Accepted)
    {
        ArmManager::instance()->setConfig(conf);
    }
}

void MainWindow::on_servoTable_itemSelectionChanged()
{
    int id = getTableCurrentServoId();
    ui->editServoWidget->setVisible(id != -1);
}

void MainWindow::on_transitionAngleSpin_valueChanged(double arg1)
{
    arg1 = armio::degToRad(arg1);

    int id = getTableCurrentServoId();
    if(id == -1)
        return;

    ServoConfig* conf = ArmManager::instance()->configAt(id);
    if(!conf)
        return;

    qint64 time = conf->calculateTransitionTimeMs(ArmManager::instance()->servoAngle(id), arg1, 0);
    disconnect(ui->transitionTimeSpin, SIGNAL(valueChanged(int)), this, SLOT(on_transitionTimeSpin_valueChanged(int)));
    ui->transitionTimeSpin->setValue(time);
    connect(ui->transitionTimeSpin, SIGNAL(valueChanged(int)), this, SLOT(on_transitionTimeSpin_valueChanged(int)));

    ArmManager::instance()->setServoAngle(id, arg1, time);
}

void MainWindow::on_transitionTimeSpin_valueChanged(int arg1)
{
    int id = getTableCurrentServoId();
    if(id == -1)
        return;

    ArmManager::instance()->setServoAngle(id, armio::degToRad(ui->transitionAngleSpin->value()), arg1);
}

void MainWindow::on_loadConfigBtn_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open servo config"), QDir::currentPath(), tr("Servo Config Files (*.sconf)"));
    if(fileName.isEmpty())
        return;

    if(!ArmManager::instance()->loadConfig(fileName)) {
        QMessageBox::warning(this, "Error", "Load servo config failed!");
    }
    else m_settings->setValue("CurrentConfigPath", QVariant::fromValue(fileName));
}

void MainWindow::on_saveConfigBtn_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save servo config"), QDir::currentPath(), tr("Servo Config Files (*.sconf)"));
    if(fileName.isEmpty())
        return;

    if(!ArmManager::instance()->saveConfig(fileName)) {
        QMessageBox::warning(this, "Error", "Load servo config failed!");
    }
    else m_settings->setValue("CurrentConfigPath", QVariant::fromValue(fileName));
}

void MainWindow::on_enableServoBtn_clicked()
{
    bool bVal = !ArmManager::instance()->isServoEnabled();
    if(bVal) {
        ArmManager::instance()->enableAllServo();
        ui->enableServoBtn->setText("Disable movement");
    }
    else {
        ArmManager::instance()->disableAllServo();
        ui->enableServoBtn->setText("Enable movement");
    }
}

ServoMapperFrame *MainWindow::createServoMapper(ServoMapperFrame::MapperType type)
{
    QListWidgetItem* item = new QListWidgetItem(ui->joyMappingsList);
    ServoMapperFrame* frame = new ServoMapperFrame(this, nullptr);
    frame->setScriptEngine(m_scriptEngine);
    frame->setDinputCtrl(m_dinputCtrl);
    frame->setMapperType(type);
    item->setSizeHint(frame->sizeHint());
    ui->joyMappingsList->setItemWidget(item,frame);
    return frame;
}

bool MainWindow::loadServoMapping(const QString &path)
{
    ui->joyMappingsList->clear();

    QFile file(path);
    if(!file.open(QIODevice::ReadOnly)) {
        qCritical() << "File open failed: " << path;
        return false;
    }

    QByteArray fileData = file.readAll();
    file.close();

    QJsonDocument doc = QJsonDocument::fromJson(fileData);
    QJsonArray arr = doc.array();

    for(int i = 0; i < arr.size(); i++) {
        ServoMapperFrame* frame = createServoMapper(ServoMapperFrame::kUnknown);
        frame->loadMapping(arr[i].toObject());
    }
    return true;
}

bool MainWindow::saveServoMapping(const QString &path) const
{
    QJsonArray arr;
    for(int i = 0; i < ui->joyMappingsList->count(); i++) {
        QListWidgetItem* item = ui->joyMappingsList->item(i);
        if(!item)
            continue;
        ServoMapperFrame* frame = (ServoMapperFrame*)ui->joyMappingsList->itemWidget(item);
        if(!frame)
            continue;

        QJsonObject obj;
        if(!frame->saveMapping(obj))
            continue;
        arr.append(obj);
    }

    QJsonDocument doc;
    doc.setArray(arr);

    QFile file(path);
    if(!file.open(QIODevice::WriteOnly)) {
        qCritical() << "File open failed: " << path;
        return false;
    }

    file.write(doc.toJson());
    file.close();
    return true;
}

void MainWindow::on_addAxisBtn_clicked()
{
    createServoMapper(ServoMapperFrame::kDirectInputAxis);
}

void MainWindow::on_addJoystickButtonBtn_clicked()
{
    createServoMapper(ServoMapperFrame::kDirectInputButton);
}

void MainWindow::on_addShortcutBtn_clicked()
{
    createServoMapper(ServoMapperFrame::kKeyboardShortcut);
}

void MainWindow::on_loadMappingBtn_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(this, tr("Open servo mapping"), QDir::currentPath(), tr("Servo Mapping Files (*.smap)"));
    if(filePath.isEmpty())
        return;

    if(loadServoMapping(filePath))
    {
        m_settings->setValue("CurrentMappingPath", QVariant::fromValue(filePath));
    }
    else QMessageBox::warning(this, "Error", "Load failed!");
}

void MainWindow::on_saveMappingBtn_clicked()
{
    QString filePath = QFileDialog::getSaveFileName(this, tr("Save servo mapping"), QDir::currentPath(), tr("Servo Mapping Files (*.smap)"));
    if(filePath.isEmpty())
        return;

    if(saveServoMapping(filePath))
    {
        m_settings->setValue("CurrentMappingPath", QVariant::fromValue(filePath));
    }
    else QMessageBox::warning(this, "Error", "Save failed!");
}

void MainWindow::on_clearMapping_clicked()
{
    ui->joyMappingsList->clear();
}

void MainWindow::on_clearServoConfig_clicked()
{
    ArmManager::instance()->removeAllConfigs();
}

void MainWindow::on_joystickCombo_currentIndexChanged(int index)
{
    if(index == -1)
        return;

    QVariant v = ui->joystickCombo->itemData(index);
    bool bOk = false;
    int id = v.toInt(&bOk);
    if(bOk && (id >= 0) && (id < m_joysticksList.size()))
        switchToJoystick(m_joysticksList[id].name);
    else
        switchToJoystick("");
}

void MainWindow::switchToJoystick(const QString &name)
{
    if(name.isEmpty()) {
        m_dinputCtrl->release();
        bool bVal = ui->joystickCombo->blockSignals(true);
        ui->joystickCombo->setCurrentIndex(0);
        ui->joystickCombo->blockSignals(bVal);
    }

    for(int i = 0; i < m_joysticksList.size(); i++) {
        if(m_joysticksList[i].name == name) {
            m_settings->setValue("LastJoystick", QVariant::fromValue(name));
            m_dinputCtrl->create(m_joysticksList[i].id);

            bool bVal = ui->joystickCombo->blockSignals(true);

            for(int j = 0; j < ui->joystickCombo->count(); j++) {
                if(ui->joystickCombo->itemData(j).toInt() == i)
                    ui->joystickCombo->setCurrentIndex(j);
            }

            ui->joystickCombo->blockSignals(bVal);

            return;
        }
    }
    qCritical() << "Can't find joystick:" << name;
    m_dinputCtrl->release();
}

void MainWindow::on_stateTable_itemSelectionChanged()
{
    bool hasSelection = ui->stateTable->selectionModel()->hasSelection();
    ui->setStateToCurrentBtn->setEnabled(hasSelection);
    ui->setStateToZeroBtn->setEnabled(hasSelection);
    ui->removeStateBtn->setEnabled(hasSelection);
    ui->addTransitionBtn->setEnabled(hasSelection);
}

void MainWindow::updateStateTable()
{
    ServoSequence* sequence = ArmManager::instance()->currentSequence();

    b_disable_itemchanged_reaction = true;

    QSize tableSize;
    tableSize.setHeight(sequence->stateCount());
    for(int i = 0; i < sequence->stateCount(); i++)
    {
        const ServoState* state = sequence->stateAt(i);
        for(int j = 0; j < state->valueCount(); j++)
            if(state->hasValue(j))
                tableSize.setWidth(std::max<int>(tableSize.width(), j+1));
    }

    ui->stateTable->setRowCount(tableSize.height());
    ui->stateTable->setColumnCount(tableSize.width());

    for(int nCol = 0; nCol < tableSize.width(); nCol++) {
        if(!ui->stateTable->horizontalHeaderItem(nCol))
            ui->stateTable->setHorizontalHeaderItem(nCol, new QTableWidgetItem(QString::number(nCol)));
    }

    for(int nRow = 0; nRow < sequence->stateCount(); nRow++) {
        const ServoState* state = sequence->stateAt(nRow);
        if(!ui->stateTable->verticalHeaderItem(nRow))
            ui->stateTable->setVerticalHeaderItem(nRow, new QTableWidgetItem());
        ui->stateTable->verticalHeaderItem(nRow)->setText(state->name());

        for(int nCol = 0; nCol < tableSize.width(); nCol++) {
            QTableWidgetItem* item = ui->stateTable->item(nRow, nCol);
            if(!item) {
                item = new QTableWidgetItem();
                ui->stateTable->setItem(nRow, nCol, item);
            }
            if(state->hasValue(nCol))
                item->setText(QString::number(armio::radToDeg(state->value(nCol))));
            else
                item->setText("");
        }
    }

    ui->stateTable->resizeColumnsToContents();
    ui->stateTable->resizeRowsToContents();

    b_disable_itemchanged_reaction = false;
}

void MainWindow::updateTransitionsTable()
{
    ServoSequence* sequence = ArmManager::instance()->currentSequence();

    b_disable_itemchanged_reaction = true;

    int rowCount = 0;
    for(int nRow = 0; nRow < sequence->animationCount(); nRow++) {
        ServoStateAnimation* anim = qobject_cast<ServoStateAnimation*>(sequence->animationAt(nRow));
        if(!anim)
            continue;
        rowCount++;
    }

    ui->transitionTable->setRowCount(rowCount);

    QMetaEnum easingCurveEnum = QMetaEnum::fromType<QEasingCurve::Type>();

    int nRow = 0;
    for(int i = 0; i < sequence->animationCount(); i++) {
        ServoStateAnimation* anim = qobject_cast<ServoStateAnimation*>(sequence->animationAt(i));
        if(!anim)
            continue;

        QTableWidgetItem* timeItem = ui->transitionTable->item(nRow, 0);
        if(!timeItem) {
            timeItem = new QTableWidgetItem();
            ui->transitionTable->setItem(nRow, 0, timeItem);
        }

        QTableWidgetItem* transitionItem = ui->transitionTable->item(nRow, 1);
        if(!transitionItem) {
            transitionItem = new QTableWidgetItem();
            ui->transitionTable->setItem(nRow, 1, transitionItem);
        }

        QTableWidgetItem* curveItem = ui->transitionTable->item(nRow, 2);
        if(!curveItem) {
            curveItem = new QTableWidgetItem();
            ui->transitionTable->setItem(nRow, 2, curveItem);
        }

        timeItem->setText(QString::number(anim->duration()));

        QString transitionsText;
        for(int i = 0; i < anim->animationCount(); i++)
        {
            QPropertyAnimation* prop_anim = qobject_cast<QPropertyAnimation*>(anim->animationAt(i));
            if(!prop_anim) continue;
            ServoHandler* handler =  qobject_cast<ServoHandler*>(prop_anim->targetObject());
            if(!handler) continue;


            if(i) {
                transitionsText += ", ";
            }
            float v1 = armio::radToDeg(prop_anim->startValue().toFloat());
            float v2 = armio::radToDeg(prop_anim->endValue().toFloat());
            transitionsText.append(QString::asprintf("%d-[%.1f:%.1f]",handler->getId(), v1, v2));
        }

        transitionItem->setText(transitionsText);

        curveItem->setText(easingCurveEnum.valueToKey(anim->easingCurve().type()));

        nRow++;
    }

    ui->transitionTable->resizeColumnsToContents();
    ui->transitionTable->resizeRowsToContents();

    b_disable_itemchanged_reaction = false;
}

void MainWindow::on_addStateBtn_clicked()
{
    static int state_index = 0;
    state_index++;
    ServoState* state = new ServoState(ArmManager::instance()->currentSequence());
    state->setName(QString::asprintf("State%d",state_index));
    state->dumpZeroState();
    ArmManager::instance()->currentSequence()->addState(state);
}

void MainWindow::on_setStateToCurrentBtn_clicked()
{
    int rowIndex = ui->stateTable->selectionModel()->currentIndex().row();
    ServoState* state = ArmManager::instance()->currentSequence()->stateAt(rowIndex);
    state->dumpCurrentState();
    ArmManager::instance()->currentSequence()->statesChanged();
}

void MainWindow::on_setStateToZeroBtn_clicked()
{
    int rowIndex = ui->stateTable->selectionModel()->currentIndex().row();
    ServoState* state = ArmManager::instance()->currentSequence()->stateAt(rowIndex);
    state->dumpZeroState();
    ArmManager::instance()->currentSequence()->statesChanged();
}

void MainWindow::on_removeStateBtn_clicked()
{
    int rowIndex = ui->stateTable->selectionModel()->currentIndex().row();
    ServoState* state = ArmManager::instance()->currentSequence()->stateAt(rowIndex);
    ArmManager::instance()->currentSequence()->removeState(state);
}

void MainWindow::on_clearStatesBtn_clicked()
{
    ArmManager::instance()->currentSequence()->removeAllStates();
}

void MainWindow::on_stateTable_itemChanged(QTableWidgetItem *item)
{
    if(b_disable_itemchanged_reaction)
        return;

    int rowIndex = item->row();
    ServoState* state = ArmManager::instance()->currentSequence()->stateAt(rowIndex);

    bool bOk = false;
    double val = item->text().toDouble(&bOk);
    if(bOk) {
        state->setValue(item->column(), armio::degToRad(val));
    }
    else {
        if(state->hasValue(item->column()))
            item->setText(QString::number(armio::radToDeg(state->value(item->column()))));
        else
            item->setText("");
    }
}

void MainWindow::on_transitionTable_itemSelectionChanged()
{
    bool hasSelection = ui->transitionTable->selectionModel()->hasSelection();
    ui->removeTransitionBtn->setEnabled(hasSelection);
}

void MainWindow::on_addTransitionBtn_clicked()
{
    int stateIndex = ui->stateTable->selectionModel()->currentIndex().row();
    ServoState* state = ArmManager::instance()->currentSequence()->stateAt(stateIndex);
    ArmManager::instance()->currentSequence()->addTransition(state);
}

void MainWindow::on_removeTransitionBtn_clicked()
{
    int index = ui->transitionTable->selectionModel()->currentIndex().row();

    auto animation = ArmManager::instance()->currentSequence()->animationAt(index);
    ArmManager::instance()->currentSequence()->removeAnimation(animation);
    emit ArmManager::instance()->currentSequence()->animationsChanged();
}

void MainWindow::on_clearTransitionsBtn_clicked()
{
    ArmManager::instance()->currentSequence()->clear();
    emit ArmManager::instance()->currentSequence()->animationsChanged();
}

void MainWindow::on_playSequenceBtn_clicked()
{
    if(ArmManager::instance()->currentSequence()->state() == QAbstractAnimation::Running)
        ArmManager::instance()->currentSequence()->stop();
    else
        ArmManager::instance()->currentSequence()->start();
}

void MainWindow::sequenceStateChanged(QAbstractAnimation::State newState, QAbstractAnimation::State oldState)
{
    if(newState == QAbstractAnimation::Running)
        ui->playSequenceBtn->setText("Stop sequence");
    else
        ui->playSequenceBtn->setText("Play sequence");
}

void MainWindow::onEasingCurveChanged()
{
    ArmManager::instance()->currentSequence()->setEasingCurve(ui->easingCurveWidget->curve());
}

void MainWindow::on_clearMessagesBtn_clicked()
{
    ui->recvMsgBrowser->clear();
}

void MainWindow::on_loadSequenceBtn_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open servo sequence"), QDir::currentPath(), tr("Servo Sequence Files (*.sseq)"));
    if(fileName.isEmpty())
        return;

    if(!ArmManager::instance()->currentSequence()->load(fileName)) {
        QMessageBox::warning(this, "Error", "Load servo sequence failed!");
    }
    else m_settings->setValue("CurrentServoSequencePath", QVariant::fromValue(fileName));
}

void MainWindow::on_saveSequenceBtn_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save servo sequence"), QDir::currentPath(), tr("Servo Sequence Files (*.sseq)"));
    if(fileName.isEmpty())
        return;

    if(!ArmManager::instance()->currentSequence()->save(fileName)) {
        QMessageBox::warning(this, "Error", "Save servo sequence failed!");
    }
    else m_settings->setValue("CurrentServoSequencePath", QVariant::fromValue(fileName));
}

void MainWindow::on_transitionMultSpin_valueChanged(double arg1)
{
    ServoConfig::TransitionMult = arg1;
    m_settings->setValue("tmul", QVariant::fromValue(arg1));
}

void MainWindow::on_transitionAddSpin_valueChanged(int arg1)
{
    ServoConfig::AdditionalTime = arg1;
    m_settings->setValue("tadd", QVariant::fromValue(arg1));
}
