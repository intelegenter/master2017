#-------------------------------------------------
#
# Project created by QtCreator 2017-05-12T22:28:56
#
#-------------------------------------------------

QT       += core gui serialport qml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ArmApp
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    armconnection.cpp \
    armio_utils.cpp \
    armmanager.cpp \
    armservo.cpp \
    widgets/servoconfigeditdialog.cpp \
    servocommand.cpp \
    widgets/servostateframe.cpp \
    servomapper.cpp \
    dinput/dinput_utils.cpp \
    dinput/dinputctrl.cpp \
    widgets/servomapperframe.cpp \
    widgets/servocommandframe.cpp \
    widgets/shortcutgrabber.cpp \
    servosequence.cpp \
    widgets/easingcurvewidget.cpp

HEADERS  += mainwindow.h \
    armconnection.h \
    armio_utils.h \
    armmanager.h \
    armservo.h \
    widgets/servoconfigeditdialog.h \
    servocommand.h \
    widgets/servostateframe.h \
    servomapper.h \
    dinput/dinput_utils.h \
    dinput/dinputctrl.h \
    widgets/servomapperframe.h \
    widgets/servocommandframe.h \
    widgets/shortcutgrabber.h \
    servosequence.h \
    widgets/easingcurvewidget.h

INCLUDEPATH += $$PWD/../../armlib

INCLUDEPATH += $$PWD/../../../dependencies/include
win32:LIBS += -ldinput8 -ldxguid

FORMS    += mainwindow.ui \
    widgets/servoconfigeditdialog.ui \
    widgets/servostateframe.ui \
    widgets/servomapperframe.ui \
    widgets/servocommandframe.ui \
    widgets/easingcurvewidget.ui
