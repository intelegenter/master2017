#ifndef ARMSERVO_H
#define ARMSERVO_H

#include <QObject>
#include "armio_utils.h"

//class ArmManager;
class ServoConfig
{
public:
    static float TransitionMult;
    static qint64 AdditionalTime;
public:
    ServoConfig();
    ServoConfig(uint8_t id);

    void setServoId(uint8_t id);
    uint8_t servoId() const;

    void setMinPulseWeight(uint16_t val);
    uint16_t minPulseWeight() const;

    void setMaxPulseWeight(uint16_t val);
    uint16_t maxPulseWeight() const;

    void setPulseFrequency(uint16_t val);
    uint16_t pulseFrequency() const;

    void setEstimatedSpeed(float fRadPerSec);
    float estimatedSpeed() const;

    void setMinimumAngle(float val);
    float minimumAngle() const;

    void setMaximumAngle(float val);
    float maximumAngle() const;

    bool saveJSon(QJsonObject& obj);
    bool loadJSon(const QJsonObject& obj);

    qint64 calculateTransitionTimeMs(float ang1, float ang2, qint64 additionalTime);

    const ArmMsgData_ServoDecl& decl() const;
private:
    ArmMsgData_ServoDecl servo_decl;
};

//class ServoState
//{
//public:
//    void setCurrentAngle(float fVal);
//    float currentAngle() const;
////signals:
////    void currentAngleChanged(float fVal);
//private:
//    float current_angle = 0.f;
//    float transition_angle = 0.f;
//    qint64 transition_time = 0;
//};

//class ArmConfig : public QObject
//{
//public:

//};

//class ArmState : public QObject
//{
//    Q_OBJECT
//public:
//    ArmState(ArmConfig* config);
//    void reset(ArmConfig* config);
//private:
//    QVector<ServoState*> servos;
//};

#endif // ARMSERVO_H
