#ifndef ARMIO_UTILS_H
#define ARMIO_UTILS_H

#include <armio.h>
#include <QSharedPointer>
#include <QJsonObject>

class ArmMessage {
public:
    typedef QSharedPointer<ArmMessage> Pointer;
    static ArmMessage::Pointer Create(ArmMsgDataType datatype);

public:
    ArmMessage();
    ArmMessage(ArmMsgDataType datatype);
    ~ArmMessage();

    void create(ArmMsgDataType datatype);
    bool createFromByteArray(QByteArray& arr);
    bool createFromJson(const QJsonObject& obj);

    QByteArray toByteArray() const;
    QString toString() const;
    bool toJson(QJsonObject& obj);

    void release();

    const ArmMsg& getMessage();

    template<typename T>
    T* getData(ArmMsgDataType datatype) {
        if(message.data_type != datatype)
            return nullptr;
        return (T*)message.data.pData;
    }

private:
    Q_DISABLE_COPY(ArmMessage)
    ArmMsg message;
};

namespace armio
{
    float angleNormalize2PI(float fRad);
    float angleNormalizePI(float fRad);
    float angleClamp(float fRad, float fMin, float fMax);
    int16_t angleToShort(float fRad);
    float shortToAngle(int16_t iVal);
    float degToRad(float degrees);
    float radToDeg(float radians);
    float getShortError();
}

Q_DECLARE_METATYPE( ArmMessage::Pointer )
#endif // ARMIO_UTILS_H
