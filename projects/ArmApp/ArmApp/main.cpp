#include "mainwindow.h"
#include <QApplication>
#include <QDebug>
#include <limits>
#include <QStyleFactory>
#include "armio_utils.h"
volatile uint32_t ArmIoAllocateCounter;


int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Home");
    QCoreApplication::setApplicationName("Control system for controlling the array of servo drives(\"CSCASD\", ru: \"СКУМС\")");

    QApplication a(argc, argv);
    QApplication::setStyle(QStyleFactory::create("Fusion"));
//    QPalette palette;
//    palette.setColor(QPalette::Window, QColor(53,53,53));
//    palette.setColor(QPalette::WindowText, Qt::white);
//    palette.setColor(QPalette::Base, QColor(15,15,15));
//    palette.setColor(QPalette::AlternateBase, QColor(53,53,53));
//    palette.setColor(QPalette::ToolTipBase, Qt::white);
//    palette.setColor(QPalette::ToolTipText, Qt::white);
//    palette.setColor(QPalette::Text, Qt::white);
//    palette.setColor(QPalette::Button, QColor(53,53,53));
//    palette.setColor(QPalette::ButtonText, Qt::white);
//    palette.setColor(QPalette::BrightText, Qt::red);
//    palette.setColor(QPalette::Highlight, Qt::lightGray);
//    palette.setColor(QPalette::HighlightedText, Qt::black);
//    palette.setColor(QPalette::Disabled, QPalette::Text, Qt::darkGray);
//    palette.setColor(QPalette::Disabled, QPalette::ButtonText, Qt::darkGray);
//    qApp->setPalette(palette);

    MainWindow w;
    w.show();

    return a.exec();
}
