#ifndef ARMCONNECTION_H
#define ARMCONNECTION_H

#include <QObject>
#include <QSerialPort>
#include <QTimer>
#include <QMutex>
#include "armio_utils.h"

class ArmConnection : public QObject
{
    Q_OBJECT
public:
    explicit ArmConnection(QObject *parent = 0);

    QString portName() const;
    void setPortName(const QString &portName);

    QSerialPort::BaudRate baudRate() const;
    void setBaudRate(const QSerialPort::BaudRate &baudRate);

    QSerialPort::DataBits dataBits() const;
    void setDataBits(const QSerialPort::DataBits &dataBits);

    QSerialPort::Parity parity() const;
    void setParity(const QSerialPort::Parity &parity);

    QSerialPort::StopBits stopBits() const;
    void setStopBits(const QSerialPort::StopBits &stopBits);

    bool isConnected() const {return m_bConnected;}

signals:
    void connectionEstablished();
    void connectionLost();
    void connectionError(QSerialPort::SerialPortError errcode, QString errorText);
    void messageReceived(ArmMessage::Pointer message);
    void kbytesRW(float writeSpeed, float readSpeed);
    void messageSended(ArmMessage::Pointer message);
public slots:
    bool sendMessage(ArmMessage::Pointer message);
    bool start();
    bool stop();
protected slots:
    void onWatchDogTimerTimeout();
    void onPingTimerTimeout();
    void onRWTimerTimeout();
    void onSerialPortError(QSerialPort::SerialPortError errcode);
    void onReadyRead();
    void parseDataStream();
private:
    QString m_portName = "Unknown";
    QSerialPort::BaudRate m_baudRate = QSerialPort::Baud115200;
    QSerialPort::DataBits m_dataBits = QSerialPort::Data8;
    QSerialPort::Parity m_parity = QSerialPort::NoParity;
    QSerialPort::StopBits m_stopBits = QSerialPort::OneStop;

    quint32 m_bytesWritten = 0;
    quint32 m_bytesReaded = 0;
    float m_prevWrSpeed = 0;
    float m_prevRdSpeed = 0;

    QSerialPort* m_serialPort = nullptr;
    QByteArray m_dataStream;
    QTimer* m_watchDogTimer = nullptr;
    QTimer* m_pingTimer = nullptr;
    QTimer* m_rwUpdateTimer = nullptr;
//    QTimer* m_readTimer = nullptr;
    QMutex m_syncMutex;
    bool m_bConnected = false;
};

#endif // ARMCONNECTION_H
