#ifndef SERVOSEQUENCE_H
#define SERVOSEQUENCE_H

#include <QObject>
#include <QVector>
#include <QEasingCurve>
#include <QParallelAnimationGroup>
#include <QSequentialAnimationGroup>

class ServoState : public QObject {
    Q_OBJECT
public:
    explicit ServoState(QObject* parent = 0);
    void dumpCurrentState();
    void dumpZeroState();

    void setValue(int id, float value);
    float value(int id) const;
    bool hasValue(int id) const;
    int valueCount() const;

    QString name() const;
    void setName(const QString &name);

private:
    QVector<float> m_servoValues;
    QString m_name;
};

class ServoStateAnimation : public QParallelAnimationGroup
{
    Q_OBJECT
public:
    explicit ServoStateAnimation(QObject* parent = 0);
    void createFromStates(ServoState* state1, ServoState* state2);
    void createFromState(ServoState* state);

    void setEasingCurve(const QEasingCurve& curve);
    QEasingCurve easingCurve() const;
private:
    QEasingCurve m_easing;
};

class ServoSequence : public QSequentialAnimationGroup
{
    Q_OBJECT
public:
    explicit ServoSequence(QObject *parent = 0);

    ServoState* addCurrentState();
    ServoState* addZeroState();
    void addState(ServoState* state);
    void removeState(ServoState* state);
    void removeAllStates();

    ServoStateAnimation* addTransition(ServoState* nextState);

    const ServoState* stateAt(int idx) const;
    ServoState* stateAt(int idx);
    int stateCount() const;

    bool save(const QString& path) const;
    bool save(QJsonObject& obj) const;
    bool load(const QString& path);
    bool load(const QJsonObject& obj);

    QEasingCurve easingCurve() const;
    void setEasingCurve(const QEasingCurve &easingCurve);

signals:
    void statesChanged();
    void animationsChanged();
public slots:
private:
    ServoState* m_lastState = nullptr;
    QList<ServoState*> m_states;
    QEasingCurve m_easingCurve;
};

#endif // SERVOSEQUENCE_H
