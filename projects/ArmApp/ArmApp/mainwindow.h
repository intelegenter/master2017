#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QSettings>
#include <QJSEngine>
#include "armconnection.h"
#include "armmanager.h"
#include "dinput/dinputctrl.h"
#include "widgets/servomapperframe.h"

namespace Ui {
class MainWindow;
}
class QTableWidgetItem;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    int getTableCurrentServoId() const;
private slots:
    void updateSerialPortData();
    void resetConnection();
    void onMessageReceived(ArmMessage::Pointer message);
    void onConnectionEstablished();
    void onConnectionLost();
    void onConnectionError(QSerialPort::SerialPortError errcode, QString errorText);
    void updateServoTable();
    void updateServoTableLater();
    void loadItemToServoTable(int row, int col, int servoId);
    void onSerialIO(float wr, float rd);
    void onServoAngleChanged(int id, float angle);
    void onConfigChanged();
    void updateStateTable();
    void updateTransitionsTable();
    void sequenceStateChanged(QAbstractAnimation::State newState, QAbstractAnimation::State oldState);
    void onEasingCurveChanged();
private slots:
    void on_connectBtn_clicked();

    void on_sendRawMessageBtn_clicked();

    void on_armMessageSelectorCombo_currentIndexChanged(int index);

    void on_addServoBtn_clicked();

    void on_editCurrentServo_clicked();

    void on_servoTable_itemSelectionChanged();

    void on_transitionAngleSpin_valueChanged(double arg1);

    void on_transitionTimeSpin_valueChanged(int arg1);

//    void on_beginTransitionBtn_clicked();

//    void on_endTransitionBtn_clicked();

    void on_loadConfigBtn_clicked();

    void on_saveConfigBtn_clicked();

    void on_enableServoBtn_clicked();

    void on_addAxisBtn_clicked();

    void on_addJoystickButtonBtn_clicked();

    void on_addShortcutBtn_clicked();

    void on_loadMappingBtn_clicked();

    void on_saveMappingBtn_clicked();

    void on_clearMapping_clicked();

    void on_clearServoConfig_clicked();
    void on_joystickCombo_currentIndexChanged(int index);

    void on_stateTable_itemSelectionChanged();

    void on_addStateBtn_clicked();

    void on_setStateToCurrentBtn_clicked();

    void on_setStateToZeroBtn_clicked();

    void on_removeStateBtn_clicked();

    void on_clearStatesBtn_clicked();

    void on_stateTable_itemChanged(QTableWidgetItem *item);

    void on_transitionTable_itemSelectionChanged();

    void on_addTransitionBtn_clicked();

    void on_removeTransitionBtn_clicked();

    void on_clearTransitionsBtn_clicked();

    void on_playSequenceBtn_clicked();

    void on_clearMessagesBtn_clicked();

    void on_loadSequenceBtn_clicked();

    void on_saveSequenceBtn_clicked();

    void on_transitionMultSpin_valueChanged(double arg1);

    void on_transitionAddSpin_valueChanged(int arg1);

private:
    ServoMapperFrame* createServoMapper(ServoMapperFrame::MapperType type);
    bool loadServoMapping(const QString& path);
    bool saveServoMapping(const QString& path) const;
    void loadSettings();
    void switchToJoystick(const QString& name);
private:
    Ui::MainWindow *ui;

    QList<DInputCtrl::JoystickDecl> m_joysticksList;

    bool b_disable_itemchanged_reaction = false;

    ArmConnection* m_connection = nullptr;
    QSettings* m_settings = nullptr;
    QTimer* update_servo_table_timer;
    DInputCtrl* m_dinputCtrl = nullptr;
    QJSEngine* m_scriptEngine = nullptr;
};

#endif // MAINWINDOW_H
