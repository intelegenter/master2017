#include "armconnection.h"
#include <QDebug>
#include <QThread>

ArmConnection::ArmConnection(QObject *parent) :
    QObject(parent), m_syncMutex(QMutex::Recursive)
{
    m_watchDogTimer = new QTimer(this);
    m_watchDogTimer->setSingleShot(true);
    m_watchDogTimer->setInterval(2000);
    connect(m_watchDogTimer, &QTimer::timeout,
            this, &ArmConnection::onWatchDogTimerTimeout);

    m_pingTimer = new QTimer(this);
    m_pingTimer->setSingleShot(false);
    m_pingTimer->setInterval(500);
    connect(m_pingTimer,&QTimer::timeout,
            this, &ArmConnection::onPingTimerTimeout);

    m_rwUpdateTimer = new QTimer(this);
    m_rwUpdateTimer->setSingleShot(false);
    m_rwUpdateTimer->setInterval(150);
    connect(m_rwUpdateTimer,&QTimer::timeout,
            this, &ArmConnection::onRWTimerTimeout);
    m_rwUpdateTimer->start();

//    m_readTimer = new QTimer(this);
//    m_readTimer->setSingleShot(false);
//    m_readTimer->setInterval(500);
//    connect(m_readTimer,&QTimer::timeout,
//            this, &ArmConnection::onReadyRead);

    m_serialPort = new QSerialPort(this);
    connect(m_serialPort, &QSerialPort::readyRead,
            this, &ArmConnection::onReadyRead);
    connect(m_serialPort, &QSerialPort::errorOccurred,
            this, &ArmConnection::onSerialPortError);
}

bool ArmConnection::sendMessage(ArmMessage::Pointer message)
{
    QMutexLocker locker(&m_syncMutex);

    if(!m_serialPort->isOpen()) {
//        qCritical() << "serial port is closed!" << m_serialPort->errorString();
        return false;
    }

    QByteArray barr = message->toByteArray();
    int nWritten = m_serialPort->write(barr);
    if((nWritten == -1) || (nWritten != barr.size())) {
        qCritical() << "Serial port write error!";
        return false;
    }

    if(!m_serialPort->flush()) {
        qCritical() << "Serial port flush error!";
        return false;
    }

    if(!m_serialPort->waitForBytesWritten(500)) {
        qCritical() << "Serial port waitForBytesWritten failed!";
        return false;
    }

    m_bytesWritten += nWritten;
    emit messageSended(message);
    return true;
}

bool ArmConnection::start()
{
    QMutexLocker locker(&m_syncMutex);

    stop();

    m_serialPort->setPortName(m_portName);

    if(!m_serialPort->open(QIODevice::ReadWrite))
    {
        qCritical() << "SerialPort open failed:" << m_serialPort->errorString() << m_serialPort->error();
        return false;
    }

    bool bRes = false;
    if(!m_serialPort->setBaudRate(m_baudRate)) {
        qCritical() << "Failed to set baud rate!";
    }
    else if(!m_serialPort->setDataBits(m_dataBits)) {
        qCritical() << "Failed to set data bits!";
    }
    else if(!m_serialPort->setParity(m_parity)) {
        qCritical() << "Failed to set data bits!";
    }
    else if(!m_serialPort->setStopBits(m_stopBits)) {
        qCritical() << "Failed to set data bits!";
    } else {
        bRes = true;
    }

    if(!bRes || !m_serialPort->isOpen()) {
        qCritical() << "SerialPort setup failed" << m_serialPort->errorString() << m_serialPort->error();
        m_serialPort->close();
        return false;
    }

//    QThread::sleep(2);



//    QThread::sleep(1);
    m_watchDogTimer->start();   
    qDebug() << "Serial port started:" << m_portName << m_baudRate << m_dataBits << m_parity << m_stopBits;
    m_bConnected = true;

    ArmMessage::Pointer msg = ArmMessage::Create(kArmMsgData_Service);
    msg->getData<ArmMsgData_Service>(kArmMsgData_Service)->code = kServiceCode_StartConnection;
    if(sendMessage(msg))
        emit connectionEstablished();
    else return false;
//    m_readTimer->start();
    return true;
}

bool ArmConnection::stop()
{
    QMutexLocker locker(&m_syncMutex);

    m_watchDogTimer->stop();
//    m_readTimer->stop();

    if(m_serialPort->isOpen()) {        
        ArmMessage::Pointer msg = ArmMessage::Create(kArmMsgData_Service);
        msg->getData<ArmMsgData_Service>(kArmMsgData_Service)->code = kServiceCode_EndConnection;
        sendMessage(msg);
        m_serialPort->close();        
        qDebug() << "Serial port stopped!";
    }

    m_dataStream.clear();

    if(m_bConnected)
        emit connectionLost();
    m_bConnected = false;
    return true;
}

void ArmConnection::onWatchDogTimerTimeout()
{
//    stop();
}

void ArmConnection::onPingTimerTimeout()
{
    if(m_serialPort->isOpen()) {
        ArmMessage::Pointer msg = ArmMessage::Create(kArmMsgData_Service);
        msg->getData<ArmMsgData_Service>(kArmMsgData_Service)->code = kServiceCode_Ping;
        sendMessage(msg);
    }
}

void ArmConnection::onRWTimerTimeout()
{
    float interval = 1000.f / float(m_rwUpdateTimer->interval());
    float writeSpeed = float(m_bytesWritten) * interval / 1024.f;
    float readSpeed = float(m_bytesReaded) * interval / 1024.f;
    m_bytesWritten = m_bytesReaded = 0;
    m_prevWrSpeed = m_prevWrSpeed * 0.3 + writeSpeed * 0.7;
    m_prevRdSpeed = m_prevRdSpeed * 0.3 + readSpeed * 0.7;
    emit kbytesRW(m_prevWrSpeed, m_prevRdSpeed);
}

void ArmConnection::onSerialPortError(QSerialPort::SerialPortError errcode)
{    
    if(errcode != QSerialPort::NoError) {
        qCritical() << "SerialPort error:" << errcode << m_serialPort->errorString();

        //Disable error signals (prevent from firing another recursive call onSerialPortError in stop() function)
        disconnect(m_serialPort, &QSerialPort::errorOccurred, this, &ArmConnection::onSerialPortError);
        stop();
        connect(m_serialPort, &QSerialPort::errorOccurred, this, &ArmConnection::onSerialPortError);

        emit connectionError(errcode, m_serialPort->errorString());
    }
}

void ArmConnection::onReadyRead()
{    
    if(m_serialPort->bytesAvailable()) {
//        qDebug() << "onReadyRead";
        int oldSize = m_dataStream.size();
        m_dataStream.append(m_serialPort->readAll());
        m_bytesReaded += m_dataStream.size() - oldSize;
        parseDataStream();
        m_watchDogTimer->start();
    }
}

void ArmConnection::parseDataStream()
{
    while(!m_dataStream.isEmpty())
    {
        ArmMessage::Pointer message(new ArmMessage());
        if(!message->createFromByteArray(m_dataStream)) {
            qDebug() << "Parse failed, size=" << m_dataStream.size();
            break;
        }

        qDebug() << "Message received!";
        emit messageReceived(message);
    }
}

QSerialPort::StopBits ArmConnection::stopBits() const
{
    return m_stopBits;
}

void ArmConnection::setStopBits(const QSerialPort::StopBits &stopBits)
{
    m_stopBits = stopBits;
}

QSerialPort::Parity ArmConnection::parity() const
{
    return m_parity;
}

void ArmConnection::setParity(const QSerialPort::Parity &parity)
{
    m_parity = parity;
}

QSerialPort::DataBits ArmConnection::dataBits() const
{
    return m_dataBits;
}

void ArmConnection::setDataBits(const QSerialPort::DataBits &dataBits)
{
    m_dataBits = dataBits;
}

QSerialPort::BaudRate ArmConnection::baudRate() const
{
    return m_baudRate;
}

void ArmConnection::setBaudRate(const QSerialPort::BaudRate &baudRate)
{
    m_baudRate = baudRate;
}

QString ArmConnection::portName() const
{
    return m_portName;
}

void ArmConnection::setPortName(const QString &portName)
{
    m_portName = portName;
}
