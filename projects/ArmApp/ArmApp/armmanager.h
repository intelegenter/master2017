#ifndef ARMMANAGER_H
#define ARMMANAGER_H

#include <QObject>
#include <QTimer>
#include <QVector>
#include <QJsonDocument>
#include <QElapsedTimer>
#include "armservo.h"
#include "armconnection.h"
#include "servosequence.h"

class ServoHandler : public QObject {
    Q_OBJECT
    Q_PROPERTY(float angle READ getAngle WRITE setAngle NOTIFY angleChanged)
public:
    ServoHandler(int id, QObject* parent = nullptr);

    int getId() const;

    float getAngle() const;
    void setAngle(float value);

signals:
    void angleChanged(float val);
protected slots:
    void onManagerAngleChanged(int id, float angle);
private:
    int id = -1;
    float angle = 0.f;
};

class ArmManager : public QObject
{
    Q_OBJECT   
    struct ServoData {
        ServoData() {}
        ~ServoData() {}

        ServoConfig config;
        float current_angle = 0.f;
        float target_angle = 0.f;
        float target_speed = 0.f;
        float next_angle = 0.f;
        float estimated_speed = 0.f;
//        qint64 time = 0;
        qint64 start_time = 0;
        bool b_movement_mode = false;
        bool b_first_init = false;
    };

public:
    static ArmManager* instance();
    explicit ArmManager(QObject *parent = 0);
    ~ArmManager();

    void setConfig(const ServoConfig& config);
    ServoConfig* configAt(int idx);
    void removeAllConfigs();
    int servoCount() const {return m_servos.size();}

    ServoHandler *handler(int servoId);

    ArmConnection *connection() const;
    void setConnection(ArmConnection *connection);

    bool saveConfig(const QString& path) const;
    bool saveConfig(QJsonArray& arr) const;
    bool loadConfig(const QString& path);
    bool loadConfig(const QJsonArray& arr);
    ServoSequence *currentSequence() const;

signals:
    void configChanged();
    void servoAngleChanged(int id, float angle);
public slots:
    void enableAllServo();
    void disableAllServo();
    bool isServoEnabled() const;

    void setServoAngle(int id, float angle, qint64 time);
    void setServoSpeed(int id, float speed);

    float servoAngle(int id) const;
    float servoSpeed(int id) const;
protected slots:
    void processServo();
    void onArmConnected();
    void writeConfig(int id);
private:
    ServoData* servoDataById(int id);
    const ServoData* servoDataById(int id) const;
private:
    QTimer* m_servoMovementTimer = nullptr;
    QElapsedTimer time_measure;
    QVector<ServoData*> m_servos;
    QVector<ServoHandler*> m_handlers;
    ArmConnection* m_connection = nullptr;
    ServoSequence* m_currentSequence = nullptr;
    qint64 prev_time = 0;
    qint64 prev_real_delta = 0;
};

#endif // ARMMANAGER_H
