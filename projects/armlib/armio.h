#ifndef ARMIO_H
#define ARMIO_H

#include <stdint.h>
#include <stdlib.h>
//#include <stdio.h>
#include <string.h>
#define ARMIO_MAX_SERVOS_NUM 16

#pragma pack(push)  /* push current alignment to stack */
#pragma pack(1)     /* set alignment to 1 byte boundary */

//--------------------------DATA DECLARATION-----------------------------------
//Описывает установки для сервопривода
typedef struct {
    uint8_t     servo_id;           //servo id [0 - 15]
    uint16_t    min_pulse_weight;   //мин. ширина импульса, мкс
    uint16_t    max_pulse_weight;   //макс. ширина импульса, мкс
    uint16_t    pulse_freq;         //частота импульсов, мкс
    uint16_t    estimated_speed;    //ожидаемая угловая скорость поворота, отм./сек (1 отм. = PI / 32767)
    int16_t     min_angle;          //мин. угол, отм.   (0 - середина)
    int16_t     max_angle;          //макс. угол, отм.  (0 - середина)
} ArmMsgData_ServoDecl;

//Описывает угол поворота сервопривода
typedef struct {
    uint8_t     servo_id;           //servo id [0 - 15]
    int16_t     angle;              //абсолютный угол поворота, отм (0 - середина)
    int16_t     speed;              //ожидаемая угловая скорость поворота, отм./сек (1 отм. = PI / 32767)
} ArmMsgData_ServoState;

//Debug info string
typedef struct {
    char debug_info[256];
} ArmMsgData_DebugInfo;

//Service code
typedef enum {
    kServiceCode_Unknown = 0,
    kServiceCode_Ping,
    kServiceCode_StartConnection,
    kServiceCode_EndConnection,
    kServiceCode_ExecuteServo,
    kServiceCode_ExecuteInterrupt,
    kServiceCode_Last,
} ArmMsgServiceCode;

//Для упрощения обмена простой информацией и коммандами (чтоб не городить по структуре на каждую комманду)
typedef struct {
    int32_t code;
    int32_t data1;
    int32_t data2;
} ArmMsgData_Service;

typedef enum {
    kArmMsgData_Unknown = 0,
    kArmMsgData_ServoDecl,
    kArmMsgData_ServoState,
    kArmMsgData_DebugInfo,
    kArmMsgData_Service,
    kArmMsgData_Echo,
    kArmMsgData_Last
} ArmMsgDataType;

typedef union {
    void* pData;
    ArmMsgData_ServoDecl* pServoDecl;
    ArmMsgData_ServoState* pServoState;
    ArmMsgData_Service* pServiceCommand;
    ArmMsgData_DebugInfo* pDebugCommand;
} ArmMsgData;

//--------------------------MESSAGE DECLARATION-----------------------------------
#define ARMIO_MAGIC_NUMBER 0xADCF
#define ARMIO_HEADER_SIZE 6
extern volatile uint32_t ArmIoAllocateCounter;
typedef struct {
    uint16_t magic;           //magic number (for check)
    uint8_t msg_id;                 //message index (for check)
    uint8_t data_type;              //message data type (ArmMsgDataType)
    uint16_t data_size;              //message data size
    ArmMsgData data;
} ArmMsg;

inline uint32_t GetArmMsgDataSize(int data_type) {
    switch(data_type) {
    case kArmMsgData_ServoDecl:         return sizeof(ArmMsgData_ServoDecl);
    case kArmMsgData_ServoState:        return sizeof(ArmMsgData_ServoState);
    case kArmMsgData_DebugInfo:         return sizeof(ArmMsgData_DebugInfo);
    case kArmMsgData_Service:           return sizeof(ArmMsgData_Service);
    }
    return 0;
}

inline void* CreateArmMsgData(int data_type) {
    int size = GetArmMsgDataSize(data_type);
    if(size == 0)
        return 0;
    ArmIoAllocateCounter++;
    void* pData = malloc(size);
    if(!pData)
      return 0;
    memset(pData, 0, size);
    return pData;
}

inline void DestroyArmMsgData(ArmMsg* pMsg) {
    if(pMsg->data.pData) {
        ArmIoAllocateCounter--;
        free(pMsg->data.pData);
        pMsg->data.pData = 0;
    }
}

#pragma pack(pop)
#endif
