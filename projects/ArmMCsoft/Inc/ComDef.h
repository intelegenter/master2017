// File ComDef.h

//#pragma once
#ifndef MY_COMDEF
#define MY_COMDEF
#include <stdio.h>
#include <intrinsics.h>
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_tim.h"
#include "stm32f4xx.h"
#include "stm32f4xx_it.h"
#include "stm32f4xx_hal_uart.h"
#include "armio.h"

#define TIMEOUT_PERIOD 500

#define LED3_BIT (1<<13)
#define LED4_BIT (1<<14)
#define PACK_TYPE_TEST 0x5f
#define PACK_TYPE_INIT 0x01
#define PACK_TYPE_SERVO_INFO 0x02
#define PACK_TYPE_START 0x03

#define MAX_COMMAND_BUFFER_SIZE 8
#define MAX_COM_BUFFER_SIZE 2048


#pragma pack(push)  // push current alignment to stack 
#pragma pack(1)     // set alignment to 1 byte boundary 
typedef struct
{
  ArmMsgData_ServoDecl srvDecl; //структура, содержащая инициализирующую информацию по сервоприводу
  TIM_HandleTypeDef* pTim;//указатель на таймер, к которому привязан сервопривод
  uint32_t Channel; //канал таймера. используемый сервоприводом
} ServoConfig; //структура с информацией о сервоприводе

typedef struct 
{
  ServoConfig srvConf[ARMIO_MAX_SERVOS_NUM]; // массив конфигурационных данных сервоприводов
} ArmConfig;//структура, содержащая информацию о всех сервоприводах

typedef struct
{
  float CurrentPulseWeight; //текущая ширина импульса мкс
  float ExpectedPulseWeight;//требуемая ширина импульса мкс
  float deltaPulseWeight;//приращение к ширине импульса (скорость в мкс/мс)
} ServoInfo; //структура, содержащая информацию о позиционировании сервопривода

typedef struct
{
  ServoInfo srvInfo[ARMIO_MAX_SERVOS_NUM];
} ArmPos;//структура, содержащая информацию о позиции всех сервоприводов

typedef struct
{
  ArmMsgData_ServoState srvInfo[ARMIO_MAX_SERVOS_NUM];
} NewArmPos; //структура, содержащая информацию о новой полученной позиции всех сервоприводов
typedef struct
{
  ArmMsg header;
  uint8_t data[512];
} FullMsgStruct;

#pragma pack(pop)
#define ABORT_FLG_BIT_MAGIC 0x01
#define ABORT_FLG_BIT_BODY 0x02
#define ABORT_FLG_RECEIVE_HAL_ERROR 0x04

#define RECEIVE_MODE_DATA 1
#define RECEIVE_MODE_HEADER 0

#endif
