// File ComFunc.h
#ifndef MY_COMFUNC
#define MY_COMFUNC
#include "ComDef.h"
// File ComFunc.c
void SetLed(uint16_t LedBit,uint8_t LedState);
void ToggleLed(uint16_t LedBit);
float AngleToPulseWeight(float angle,uint8_t servo_id);
int16_t PulseWeightToAngle(uint16_t PulseWeight,uint8_t servo_id);
float dAng_To_dPW(float angle,uint8_t servo_id);
void AddCommandToQueue(ArmMsg msg);
void MsgParce(ArmMsg msg);
void StartParce();
void InitServos();
void SendDebugMessage(const char* DebugStr);
void SendMessage(ArmMsg* msg);
void StartServos();
void StopServos();
void SetTimArrByServoId(uint8_t ServoId);
TIM_HandleTypeDef* GetTimArrByServoId(uint8_t ServoId);
void SetPWonTim(uint8_t ServoId,uint16_t PulseWeight);
void MyError();
//uint8_t* SwitchBuffer();

#endif