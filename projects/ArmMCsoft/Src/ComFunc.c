// File ComFunc.c
#include "ComFunc.h"
#include "ComDef.h"


extern volatile uint16_t CurrMsgStrArrInd;
//ArmMsg* CommandBuffer[MAX_COMMAND_BUFFER_SIZE];
extern volatile FullMsgStruct FullMsgStrArr[MAX_COM_BUFFER_SIZE];
void SetLed(uint16_t LedBit,uint8_t LedState)
{       
  if(LedState) GPIOG->ODR |= LedBit;
  else GPIOG->ODR &= ~LedBit;
};

void ToggleLed(uint16_t LedBit)
{GPIOG->ODR ^= LedBit;
};

float AngleToPulseWeight(float angle,uint8_t servo_id)
{
 extern volatile ArmConfig cfg;
 volatile float d=cfg.srvConf[servo_id].srvDecl.max_pulse_weight-cfg.srvConf[servo_id].srvDecl.min_pulse_weight;
 volatile float coeff=32764.0/d;
 return ((angle+16382.0)/coeff+(float)cfg.srvConf[servo_id].srvDecl.min_pulse_weight);
 
};

int16_t PulseWeightToAngle(uint16_t PulseWeight,uint8_t servo_id)
{ 
 extern ArmConfig cfg;
 float d=cfg.srvConf[servo_id].srvDecl.max_pulse_weight-cfg.srvConf[servo_id].srvDecl.min_pulse_weight;
 float coeff=32764.0/d;
 return (int16_t)(((float)PulseWeight-(float)cfg.srvConf[servo_id].srvDecl.min_pulse_weight)*coeff);
};

float dAng_To_dPW(float angle,uint8_t servo_id)
{
  extern ArmConfig cfg;
  float d=cfg.srvConf[servo_id].srvDecl.max_pulse_weight-cfg.srvConf[servo_id].srvDecl.min_pulse_weight;
  float coeff=32764.0/d;
  return ((float)angle/coeff);
};

void AddCommandToQueue(ArmMsg msg)
{
  if(CurrMsgStrArrInd>=MAX_COMMAND_BUFFER_SIZE)
    return;
  //DISABLE INTERRUPTS 
  uint32_t IRQstate;
  IRQstate=__get_PRIMASK();
  __disable_interrupt();
  
  FullMsgStrArr[CurrMsgStrArrInd].header=msg;
  //CommandBuffer[CurrMsgStrArrInd]=malloc(sizeof(msg));
  memcpy((void*)FullMsgStrArr[CurrMsgStrArrInd].data,(void*)msg.data.pData,FullMsgStrArr[CurrMsgStrArrInd].header.data_size);
  FullMsgStrArr[CurrMsgStrArrInd].header.data.pData=(void*)FullMsgStrArr[CurrMsgStrArrInd].data;
  //CommandBuffer[CurrMsgStrArrInd]->data.pData=CreateArmMsgData(CommandBuffer[CurrMsgStrArrInd]->data_type);
  //memcpy(CommandBuffer[CurrMsgStrArrInd]->data.pData,msg.data.pData,CommandBuffer[CurrMsgStrArrInd]->data_size);
  //DestroyArmMsgData(&msg);
  CurrMsgStrArrInd++;
  __set_PRIMASK(IRQstate);
  //ENABLE INTERRUPTS
  
};

void MsgParce(ArmMsg msg)
{
  extern UART_HandleTypeDef huart5;
  extern ArmConfig cfg;
  extern ArmPos pos;
  extern NewArmPos NewPos;
  //SendMessage(&msg);
  if(msg.magic==ARMIO_MAGIC_NUMBER)
  {
    switch(msg.data_type)
    {
    case kArmMsgData_ServoDecl:
      {
        if((msg.data.pServoDecl->servo_id!=0) && (msg.data.pServoDecl->servo_id!=2))
        {
          uint8_t u=0;
          u++;
        }
        memcpy((void*)&cfg.srvConf[msg.data.pServoDecl->servo_id].srvDecl,msg.data.pData,msg.data_size);
        SetTimArrByServoId(msg.data.pServoDecl->servo_id);
        pos.srvInfo[msg.data.pServoDecl->servo_id].CurrentPulseWeight=((float)cfg.srvConf[msg.data.pServoDecl->servo_id].srvDecl.max_pulse_weight-
          (float)cfg.srvConf[msg.data.pServoDecl->servo_id].srvDecl.min_pulse_weight)/2;
        pos.srvInfo[msg.data.pServoDecl->servo_id].deltaPulseWeight=0;
        pos.srvInfo[msg.data.pServoDecl->servo_id].ExpectedPulseWeight=pos.srvInfo[msg.data.pServoDecl->servo_id].CurrentPulseWeight;
        SendDebugMessage("Received servo config");
//        SendMessage(&msg);
      };
      break;
    case kArmMsgData_ServoState:
      {
        memcpy((void*)&NewPos.srvInfo[msg.data.pServoState->servo_id],msg.data.pData,msg.data_size);
        
      };
      break;
    case kArmMsgData_Service:
      {
        switch(msg.data.pServiceCommand->code)
        {
        case kServiceCode_Ping:
          {
            SendDebugMessage("Ping ... OK");
          };
          break;
        case kServiceCode_StartConnection:
          {
            SendDebugMessage("Connection ... OK");
          };
          break;
        case kServiceCode_EndConnection:
          {
            StopServos();
          };
          break;
        case kServiceCode_ExecuteInterrupt:
        {
          StopServos();
        };
        break;
        case kServiceCode_ExecuteServo:
          {          
            volatile uint8_t id;
            for(id=0;id<ARMIO_MAX_SERVOS_NUM;id++)
            {
              pos.srvInfo[id].CurrentPulseWeight=pos.srvInfo[id].ExpectedPulseWeight;
              pos.srvInfo[id].ExpectedPulseWeight=AngleToPulseWeight((float)NewPos.srvInfo[id].angle,id);
              pos.srvInfo[id].deltaPulseWeight=dAng_To_dPW((float)NewPos.srvInfo[id].speed/100.0,id);    //((pos.srvInfo[id].ExpectedPulseWeight-pos.srvInfo[id].CurrentPulseWeight)/(float)NewPos.srvInfo[id].time+0.5);
              if(pos.srvInfo[id].deltaPulseWeight==0)
              {
                if(pos.srvInfo[id].ExpectedPulseWeight<pos.srvInfo[id].CurrentPulseWeight)
                {
                  pos.srvInfo[id].deltaPulseWeight=-1;
                }
                else
                { if(pos.srvInfo[id].ExpectedPulseWeight>pos.srvInfo[id].CurrentPulseWeight)
                  {
                    pos.srvInfo[id].deltaPulseWeight=1;
                  }
                  else 
                  {
                    pos.srvInfo[id].deltaPulseWeight=0;
                  };
                };
              }
            };
            StartServos();
          };
          break;
        default:
          {
          };
          break;
        };
      };
      break;
    case kArmMsgData_DebugInfo:
      {
        SendDebugMessage(msg.data.pDebugCommand->debug_info);
      };
      break;
    case kArmMsgData_Echo:
      {
        SendDebugMessage("Echo ... OK");
      };
      break;
    default:
      {
      };
      break;
    };
  };
};

void StartParce()
{
  while(CurrMsgStrArrInd>0)
  {
    MsgParce(FullMsgStrArr[0].header);
    uint32_t IRQstate;
    IRQstate=__get_PRIMASK();
    __disable_interrupt();
//INTERRUPTS DISABLE   
    //DestroyArmMsgData(CommandBuffer[0]);
    //free(CommandBuffer[0]);
    //CommandBuffer[0]=NULL;
    for(int i=0;i<CurrMsgStrArrInd-1;i++)
    {
      FullMsgStrArr[i]=FullMsgStrArr[i+1];
      //CommandBuffer[i]=CommandBuffer[i+1];
    }
    CurrMsgStrArrInd--;
    __set_PRIMASK(IRQstate);
 //INTERRUPTS ENABLE
  }
};

void InitServos()
{
  extern ArmConfig cfg;
  extern ArmPos pos;
  for(int i=0;i<ARMIO_MAX_SERVOS_NUM;i++)
  {
    cfg.srvConf[i].srvDecl.servo_id=i;
    cfg.srvConf[i].srvDecl.pulse_freq=0;
    cfg.srvConf[i].srvDecl.min_pulse_weight=0;
    cfg.srvConf[i].srvDecl.max_pulse_weight=0;
    cfg.srvConf[i].srvDecl.min_angle=0;
    cfg.srvConf[i].srvDecl.max_angle=0;
    cfg.srvConf[i].srvDecl.estimated_speed=0;
    pos.srvInfo[i].CurrentPulseWeight=0;
    pos.srvInfo[i].ExpectedPulseWeight=0;
    pos.srvInfo[i].deltaPulseWeight=0;
    cfg.srvConf[i].pTim=GetTimArrByServoId(i);
    cfg.srvConf[i].pTim->Instance->ARR=0;
  };
  cfg.srvConf[0].Channel=TIM_CHANNEL_2;
  cfg.srvConf[1].Channel=TIM_CHANNEL_3;
  cfg.srvConf[2].Channel=TIM_CHANNEL_1;
  cfg.srvConf[3].Channel=TIM_CHANNEL_2;
  cfg.srvConf[4].Channel=TIM_CHANNEL_1;
  cfg.srvConf[5].Channel=TIM_CHANNEL_3;
  cfg.srvConf[6].Channel=TIM_CHANNEL_2;
  cfg.srvConf[7].Channel=TIM_CHANNEL_1;
  cfg.srvConf[8].Channel=TIM_CHANNEL_2;
  cfg.srvConf[9].Channel=TIM_CHANNEL_1;
  
};

void SendDebugMessage(const char* DebugStr)
{
  extern uint8_t rx_buffer[MAX_COM_BUFFER_SIZE];
  extern uint8_t tx_buffer[MAX_COM_BUFFER_SIZE];
  extern volatile uint8_t bNeedTransmission;
  extern volatile uint16_t sizeoftransmission;
  ArmMsg nmsg;
  ArmMsgData_DebugInfo message;
  
  nmsg.magic=ARMIO_MAGIC_NUMBER;
  nmsg.msg_id=55;
  nmsg.data_type=kArmMsgData_DebugInfo;
  nmsg.data.pData=0;//CreateArmMsgData(kArmMsgData_DebugInfo);
  nmsg.data_size=GetArmMsgDataSize(kArmMsgData_DebugInfo);
  strcpy(message.debug_info,DebugStr);
  //strcpy(nmsg.data.pDebugCommand->debug_info,DebugStr);
  memcpy((void*)(&tx_buffer[sizeoftransmission]),(void*)&nmsg,ARMIO_HEADER_SIZE);
  memcpy((void*)(&tx_buffer[sizeoftransmission+ARMIO_HEADER_SIZE]),(void*)&message,GetArmMsgDataSize(kArmMsgData_DebugInfo));
  //memcpy((void*)(&tx_buffer[sizeoftransmission+ARMIO_HEADER_SIZE]),nmsg.data.pData,GetArmMsgDataSize(kArmMsgData_DebugInfo));
  sizeoftransmission+=nmsg.data_size+ARMIO_HEADER_SIZE;
  if(sizeoftransmission>=MAX_COM_BUFFER_SIZE)
  {
    sizeoftransmission=MAX_COM_BUFFER_SIZE-ARMIO_HEADER_SIZE-256;
  }
  bNeedTransmission=1;
  //DestroyArmMsgData(&nmsg);
};

void StartServos()
{
  extern ArmConfig cfg;  
  TIM_HandleTypeDef* pTim;
  for(uint8_t i=0;i<ARMIO_MAX_SERVOS_NUM;i++)
  {
    if(cfg.srvConf[i].srvDecl.pulse_freq==0 || cfg.srvConf[i].srvDecl.max_pulse_weight==0 ||
       cfg.srvConf[i].srvDecl.min_pulse_weight==0)
      continue;
    else
      HAL_TIM_PWM_Start(cfg.srvConf[i].pTim,cfg.srvConf[i].Channel);
  }
};
extern ArmConfig cfg;
void StopServos()
{
  TIM_HandleTypeDef* pTim;
  for(uint8_t i=0;i<ARMIO_MAX_SERVOS_NUM;i++)
  {
    HAL_TIM_PWM_Stop(cfg.srvConf[i].pTim,cfg.srvConf[i].Channel);
  }
};

void SetTimArrByServoId(uint8_t ServoId)
{
 // extern ArmConfig cfg;
  TIM_HandleTypeDef *pTim=GetTimArrByServoId(ServoId);
  if(pTim!=NULL)
  {
    pTim->Instance->ARR=cfg.srvConf[ServoId].srvDecl.pulse_freq;
  }
//  if(ServoId<2)TIM1->ARR=cfg.srvConf[ServoId].srvDecl.pulse_freq;
//        else
//          if(ServoId<4)TIM2->ARR=cfg.srvConf[ServoId].srvDecl.pulse_freq;
//          else
//            if(ServoId<6)TIM3->ARR=cfg.srvConf[ServoId].srvDecl.pulse_freq;
//            else
//              if(ServoId<7)TIM4->ARR=cfg.srvConf[ServoId].srvDecl.pulse_freq;
//              else
//                if(ServoId<9)TIM9->ARR=cfg.srvConf[ServoId].srvDecl.pulse_freq;
//                else
//                  if(ServoId==9)TIM10->ARR=cfg.srvConf[ServoId].srvDecl.pulse_freq;
//                  else
//                  {};
};
TIM_HandleTypeDef* GetTimArrByServoId(uint8_t ServoId)
{
  extern TIM_HandleTypeDef htim1;
  extern TIM_HandleTypeDef htim2;
  extern TIM_HandleTypeDef htim3;
  extern TIM_HandleTypeDef htim4;
  extern TIM_HandleTypeDef htim9;
  extern TIM_HandleTypeDef htim10;
  if(ServoId<2)return &htim1;
        else
          if(ServoId<4)return &htim2;
          else
            if(ServoId<6)return &htim3;
            else
              if(ServoId<7)return &htim4;
              else
                if(ServoId<9)return &htim9;
                else
                  if(ServoId==9)return &htim10;
                  else
                  {return NULL;};
};

void SetPWonTim(uint8_t ServoId,uint16_t PulseWeight)
{
  extern ArmConfig cfg;
  switch(cfg.srvConf[ServoId].Channel)
  {
case TIM_CHANNEL_1:
      {
         cfg.srvConf[ServoId].pTim->Instance->CCR1=PulseWeight;
      }
      break;
case TIM_CHANNEL_2:
      {
         cfg.srvConf[ServoId].pTim->Instance->CCR2=PulseWeight;
      }
      break;
case TIM_CHANNEL_3:
      {
         cfg.srvConf[ServoId].pTim->Instance->CCR3=PulseWeight;
      }
      break;
case TIM_CHANNEL_4:
      {
         cfg.srvConf[ServoId].pTim->Instance->CCR4=PulseWeight;
      }
      break;
  }
}

void SendMessage(ArmMsg* msg)
{
  extern uint8_t rx_buffer[MAX_COM_BUFFER_SIZE];
  extern uint8_t tx_buffer[MAX_COM_BUFFER_SIZE];
  extern volatile uint8_t bNeedTransmission;
  extern volatile uint16_t sizeoftransmission;
  //nmsg.magic=ARMIO_MAGIC_NUMBER;
  //nmsg.msg_id=57;
//  nmsg.data_type=kArmMsgData_DebugInfo;
//  nmsg.data.pData=CreateArmMsgData(kArmMsgData_DebugInfo);
//  nmsg.data_size=GetArmMsgDataSize(kArmMsgData_DebugInfo);
//  strcpy(nmsg.data.pDebugCommand->debug_info,DebugStr);
  memcpy((void*)(&tx_buffer[sizeoftransmission]),(void*)msg,ARMIO_HEADER_SIZE);
  if(msg->data.pData)
  {
    memcpy((void*)(&tx_buffer[sizeoftransmission+ARMIO_HEADER_SIZE]),msg->data.pData,msg->data_size);
    sizeoftransmission+=msg->data_size+ARMIO_HEADER_SIZE;
  }
  else
  {
    sizeoftransmission+=ARMIO_HEADER_SIZE;
  }
  bNeedTransmission=1;
  //DestroyArmMsgData(&msg);
}

void MyError()
{
  volatile uint8_t u8=0;
  while(u8==0)
  {
  };
}