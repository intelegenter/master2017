/**
  ******************************************************************************
  * @file    stm32f4xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx.h"
#include "stm32f4xx_it.h"

/* USER CODE BEGIN 0 */
#include "ComDef.h"
#include "ComFunc.h"
//#include <string.h>
    
//PV
extern uint8_t rx_buffer[MAX_COM_BUFFER_SIZE];
extern uint8_t tx_buffer[MAX_COM_BUFFER_SIZE];
extern volatile uint8_t bWaitReceiveData_header;

extern volatile FullMsgStruct FullMsgStrArr[MAX_COM_BUFFER_SIZE];
extern volatile uint16_t CurrMsgStrArrInd;
FullMsgStruct tmpMsg;



//extern volatile uint8_t msg_size;
extern ArmConfig cfg;
volatile ArmMsg msg;
extern ArmPos pos;
extern NewArmPos NewPos;
extern volatile uint8_t bAbortFlg;
//extern ArmMsg ReadyMsg;
extern volatile uint8_t bNeedTransmission;
extern volatile uint16_t sizeoftransmission;
volatile HAL_StatusTypeDef ITuartStatus;
uint16_t counter=0;
uint8_t bEnd=0;
//PF   

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern UART_HandleTypeDef huart5;

/******************************************************************************/
/*            Cortex-M4 Processor Interruption and Exception Handlers         */ 
/******************************************************************************/

/**
* @brief This function handles Hard fault interrupt.
*/
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
  }
  /* USER CODE BEGIN HardFault_IRQn 1 */

  /* USER CODE END HardFault_IRQn 1 */
}

/**
* @brief This function handles System tick timer.
*/
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */
  counter++;
  if(counter>=100)
  {
    counter=0;
    float EPW,CPW,D; // ExpectedPulseWeight, CurrentPulseWeight, Delta // local temporary
    //char DebugMessage[100];
    for(int16_t i=0;i<ARMIO_MAX_SERVOS_NUM;i++)
    {
      if(cfg.srvConf[i].srvDecl.pulse_freq==0)
      {
        continue;
      }
      EPW=pos.srvInfo[i].ExpectedPulseWeight;
      CPW=pos.srvInfo[i].CurrentPulseWeight;
      D=pos.srvInfo[i].deltaPulseWeight;
      if(EPW>cfg.srvConf[i].srvDecl.max_pulse_weight) 
          EPW=cfg.srvConf[i].srvDecl.max_pulse_weight;
      if(EPW<cfg.srvConf[i].srvDecl.min_pulse_weight) 
          EPW=cfg.srvConf[i].srvDecl.min_pulse_weight;
      if(((EPW)<(CPW+D) && (EPW)>(CPW-D)))
      {
        bEnd=1;
        pos.srvInfo[i].deltaPulseWeight=0;
        CPW=EPW;
      }
      if( (((EPW-CPW)<0) && (D>0)) || (((EPW-CPW)>0) && (D<0))) 
        D=-D;
      CPW+=D;
      if(CPW>cfg.srvConf[i].srvDecl.max_pulse_weight) 
        CPW=cfg.srvConf[i].srvDecl.max_pulse_weight;
      if(CPW<cfg.srvConf[i].srvDecl.min_pulse_weight) 
        CPW=cfg.srvConf[i].srvDecl.min_pulse_weight;
      pos.srvInfo[i].CurrentPulseWeight=CPW;
      SetPWonTim(i,(uint16_t)pos.srvInfo[i].CurrentPulseWeight);
    }
  }
  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  HAL_SYSTICK_IRQHandler();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
* @brief This function handles UART5 global interrupt.
*/
void UART5_IRQHandler(void)
{
  /* USER CODE BEGIN UART5_IRQn 0 */

  /* USER CODE END UART5_IRQn 0 */
  HAL_UART_IRQHandler(&huart5);
  /* USER CODE BEGIN UART5_IRQn 1 */

  /* USER CODE END UART5_IRQn 1 */
}

/* USER CODE BEGIN 1 */

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(huart); 
  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_UART_ErrorCallback could be implemented in the user file
   */ 
  uint32_t ec;
  ec=huart->ErrorCode;
  if(ec)
  {
    bWaitReceiveData_header=RECEIVE_MODE_HEADER;
    HAL_UART_Receive_IT(&huart5,rx_buffer,ARMIO_HEADER_SIZE);
    
  }
}
uint8_t tmpHdrBuf[ARMIO_HEADER_SIZE];
uint8_t tmpMsgDataBuff[256];

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(huart);
  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_UART_TxCpltCallback could be implemented in the user file
   */
  ToggleLed(LED3_BIT);
  
  if(bWaitReceiveData_header==RECEIVE_MODE_DATA)
  {
    //������� ������ ������
    //memcpy(tmpMsgDataBuff, msg.data.pData, 256);
    AddCommandToQueue(tmpMsg.header);//TODO
    
    
    bWaitReceiveData_header=RECEIVE_MODE_HEADER;
    ITuartStatus=HAL_UART_Receive_IT(&huart5,rx_buffer,ARMIO_HEADER_SIZE);
    if(ITuartStatus!=HAL_OK)
    {
      bAbortFlg|=ABORT_FLG_RECEIVE_HAL_ERROR;
    }
  }
  else
  {
    //������� ������ ������
    memcpy((void*)&tmpMsg.header, rx_buffer, ARMIO_HEADER_SIZE);
    //memcpy(tmpHdrBuf, rx_buffer, ARMIO_HEADER_SIZE);
    
    if(tmpMsg.header.magic != ARMIO_MAGIC_NUMBER) 
    {
//      assert(0);
      bAbortFlg|=ABORT_FLG_BIT_MAGIC; 
      return;
    }
    
    
    //if(GetArmMsgDataSize(msg.data_type) != msg.data_size)
    //msg.data.pData=CreateArmMsgData(msg.data_type);
    tmpMsg.header.data.pData=tmpMsg.data;
//    if(!tmpMsg.header.data.pData)
//    {
//      bAbortFlg|=ABORT_FLG_BIT_BODY;
//      return;
//    }
    
    bWaitReceiveData_header=RECEIVE_MODE_DATA;
    ITuartStatus=HAL_UART_Receive_IT(&huart5,(uint8_t*)tmpMsg.data,tmpMsg.header.data_size);
    if(ITuartStatus!=HAL_OK)
    {
      bAbortFlg|=ABORT_FLG_RECEIVE_HAL_ERROR;
      //HAL_UART_AbortReceive_IT(&huart5);
    }
    
  };
}
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(huart);
  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_UART_TxCpltCallback could be implemented in the user file
   */ 
  ToggleLed(LED4_BIT);
}
/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
