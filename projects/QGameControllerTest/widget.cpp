/***************************************************************************
 *   Copyright (C) 2014 M Wellings                                         *
 *   info@openforeveryone.co.uk                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License version 2 as     *
 *   published by the Free Software Foundation                             *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "widget.h"
#include <QTimer>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent)
{
    auto list = DirectInputGameController::EnumJoysticks();

    qDebug() << "Found " << list.size() << "controllers!";
    for(int i = 0; i < list.size(); i++)
    {
        qDebug() << list.at(i).name << list.at(i).description;
    }

    m_diCtrl = new DirectInputGameController(this);
    m_diCtrl->create(list.at(0).id);

    connect(m_diCtrl, &DirectInputGameController::axisValueChanged, this, &Widget::onAxisValueChanged);
    connect(m_diCtrl, &DirectInputGameController::buttonPressed, this, &Widget::onButtonPressed);
    //QTimer *timer = new QTimer(this);
    //connect(timer, SIGNAL(timeout()), &DIGC, SLOT(update()));
    //timer->start(1000);
//    DIGC.ReadJoystick();
//    DIGC.ReadJoystick();
    m_layout = new QVBoxLayout();
    this->setLayout(m_layout);
    buttonLabel.setText("");
    m_layout->addWidget((QWidget*)&buttonLabel);
    buttonLabel.setVisible(true);

}

Widget::~Widget()
{

}

void Widget::onButtonPressed(GUID controllerId, uint btnId, bool bPressed)
{
    QString str;
    if(bPressed)
        pressedButtons.insert(pressedButtons.end().key(),btnId);
    else
    {
        for(auto it=pressedButtons.begin();it!=pressedButtons.end();it++)
        {
            if(it.value()==btnId)
            {
                pressedButtons.remove(it.key());
                break;
            }
        }
    }
    for(auto it=pressedButtons.begin();it!=pressedButtons.end();it++)
    {
       QString tmpStr;
       tmpStr.sprintf("%d; ",it.value());
       str.append(tmpStr);
    }
    buttonLabel.setText(str);

}

void Widget::onAxisValueChanged(GUID controllerId, uint axisId, float value)
{
    setAxisValue(axisId, value);
}

AxisWidget *Widget::axisSlider(uint axisId)
{
    AxisWidget* slider = m_axis[axisId];
    if(!slider)
    {
        slider = new AxisWidget();
        slider->setAxisId(axisId);
        slider->setVisible(false);
        m_axis[axisId] = slider;
        rearrangeSliders();
    }

    return slider;
}

void Widget::setAxisValue(uint axisId, float value)
{
    AxisWidget* slider = axisSlider(axisId);
    slider->setAxisValue(value);
}

void Widget::rearrangeSliders()
{
    QList<AxisWidget*> list;
    for(auto it = m_axis.begin(); it != m_axis.end(); it++) {
        list.append(it.value());
        m_layout->removeWidget(it.value());
    }

    qSort(list.begin(), list.end(),
          [](const AxisWidget* a, const AxisWidget* b) -> bool { return a->getAxisId() < b->getAxisId(); });


    for(int i = 0; i < list.size(); i++) {
        m_layout->addWidget(list.at(i));
        list.at(i)->setVisible(true);
    }
}
