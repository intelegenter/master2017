#ifndef DINPUT_UTILS_H
#define DINPUT_UTILS_H

#include <QtWinExtras/QtWin>

namespace diutils
{
    QString toString(const HRESULT& val);
}


#endif // DINPUT_UTILS_H
