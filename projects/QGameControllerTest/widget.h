/***************************************************************************
 *   Copyright (C) 2014 M Wellings                                         *
 *   info@openforeveryone.co.uk                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License version 2 as     *
 *   published by the Free Software Foundation                             *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QSlider>
#include <QLabel>
#include <QMap>
#include <QVBoxLayout>
#include "directinputgamecontroller.h"
#include "axiswidget.h"

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
private slots:
    void onButtonPressed(GUID controllerId, uint btnId, bool bPressed);
    void onAxisValueChanged(GUID controllerId, uint axisId, float value);
private:
    AxisWidget* axisSlider(uint axisId);
    void setAxisValue(uint axisId, float value);
    void rearrangeSliders();


private:
    QLabel buttonLabel;
    QMap<uint,uint> pressedButtons;
    DirectInputGameController* m_diCtrl = nullptr;
    QMap<uint,AxisWidget*> m_axis;
    QVBoxLayout* m_layout = nullptr;
};

#endif // WIDGET_H
