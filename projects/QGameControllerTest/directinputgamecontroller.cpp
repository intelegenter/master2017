#include "directinputgamecontroller.h"
#include <QDebug>
#include <QTimer>
#include "dinput_utils.h"

BOOL CALLBACK EnumJoysticksCallback( const DIDEVICEINSTANCE* pdidInstance, VOID* pContext )
{
    QList<DirectInputGameController::JoystickDecl>* retList = (QList<DirectInputGameController::JoystickDecl>*)pContext;

    DirectInputGameController::JoystickDecl decl;
    decl.id = pdidInstance->guidInstance;
    decl.name = QString::fromWCharArray(pdidInstance->tszInstanceName);
    decl.description = QString::fromWCharArray(pdidInstance->tszProductName);
    retList->append(decl);
    return DIENUM_CONTINUE;
}

QList<DirectInputGameController::JoystickDecl> DirectInputGameController::EnumJoysticks()
{
    QList<DirectInputGameController::JoystickDecl> retList;

    LPDIRECTINPUT8 pDirectInput = nullptr;
    LPDIJOYCONFIG PreferredJoyCfg;
    HRESULT hr;
    if( FAILED( hr = DirectInput8Create( GetModuleHandle( nullptr ), DIRECTINPUT_VERSION,
                                         IID_IDirectInput8, ( VOID** )&pDirectInput, nullptr ) ) )
    {
        qDebug() << "Failed to init DirectInput!";
        return retList;
    }

//    DIJOYCONFIG PreferredJoyCfg = {0};
//    DI_ENUM_CONTEXT enumContext;
//    enumContext.pPreferredJoyCfg = &PreferredJoyCfg;
//    enumContext.bPreferredJoyCfgValid = false;

//    IDirectInputJoyConfig8* pJoyConfig = nullptr;
//    if( FAILED( hr = pDirectInput->QueryInterface(IID_IDirectInputJoyConfig8, (LPVOID*)&pJoyConfig ) ) )
//        //            return hr;
//        qDebug() << "Error 2";
//    PreferredJoyCfg->dwSize = sizeof( PreferredJoyCfg );
//    if( SUCCEEDED( pJoyConfig->GetConfig( 0, PreferredJoyCfg, DIJC_GUIDINSTANCE ) ) ) // This function is expected to fail if no joystick is attached
//    {
//        ;
//    }    //enumContext.bPreferredJoyCfgValid = true;
//    //else
//        //qDebug() << "bPreferredJoyCfgValid == false";
//    if(pJoyConfig) {
//        pJoyConfig->Release();
//        pJoyConfig = nullptr;
//    }

    if( FAILED( hr = pDirectInput->EnumDevices( DI8DEVCLASS_GAMECTRL,
                                         EnumJoysticksCallback,
                                         &retList, DIEDFL_ATTACHEDONLY ) ) )
    {
        qDebug() << "Failed to enum devices!";
        return retList;
    }

    pDirectInput->Release();

    return retList;
}

DirectInputGameController::DirectInputGameController(QObject *parent) : QObject(parent)
{
    m_timer = new QTimer(this);
    m_timer->setSingleShot(false);
    m_timer->setInterval(30);
    connect(m_timer, &QTimer::timeout, this, &DirectInputGameController::update_this_shit);

    connect(this,SIGNAL(axisValueChanged(GUID,uint,float)),this,SLOT(onAxisValueChanged(GUID,uint,float)));
    connect(this,&DirectInputGameController::buttonPressed, this, &DirectInputGameController::onButtonPressed);
}

DirectInputGameController::~DirectInputGameController()
{
    release();
}

void DirectInputGameController::create(GUID devInstanceId)
{
    release();
    //LPDIRECTINPUT8          g_pDI = nullptr;
    //LPDIRECTINPUTDEVICE8    g_pJoystick;
    HRESULT hr;
    m_deviceInstanceId = devInstanceId;
    qDebug() << "Setting up directinput";
    if( FAILED( hr = DirectInput8Create( GetModuleHandle( nullptr ), DIRECTINPUT_VERSION,
                                         IID_IDirectInput8, ( VOID** )&g_pDI, nullptr ) ) )
    {
        qDebug() << "DirectInput8Create failed!";
        return;
    }

    hr = g_pDI->CreateDevice( devInstanceId, &g_pJoystick, nullptr );
    if( FAILED( hr ) )
    {
        qDebug() << "CreateDevice failed";
        return;
    }

    //    qDebug() << "Setting data format";
        if( FAILED( hr = g_pJoystick->SetDataFormat( &c_dfDIJoystick2 ) ) )
            qDebug() << "Cannot set data format";

        if( FAILED( hr = g_pJoystick->EnumObjects( EnumObjectsCallback,
                                                   ( VOID* )this, DIDFT_ALL ) ) )
        {
            qDebug() << "Cannot to enum objects";
            return;
        }

/*        DIDEVICEINSTANCE joystickinfo;
        joystickinfo.dwSize=sizeof(joystickinfo);
        if( FAILED( hr = g_pJoystick->GetDeviceInfo(&joystickinfo)))
            qDebug() << "Error 6";
        if (hr==DIERR_INVALIDPARAM)
            qDebug() << "DIERR_INVALIDPARAM";

*/

        m_timer->start();
}

void DirectInputGameController::release()
{
    m_timer->stop();

    m_axisCount = 0;
    m_buttonsCount = 0;
    m_deviceInstanceId = GUID();
    m_axisValues.clear();
    m_buttonValues.clear();
    m_DIaxisGIIDs.clear();

    if(g_pJoystick)
    {
        g_pJoystick->Release();
        g_pJoystick = nullptr;
    }
    if(g_pDI)
    {
        g_pDI->Release();
        g_pDI = nullptr;
    }
}

void DirectInputGameController::ReadJoystick()
{
    HRESULT hr;
    DIJOYSTATE2 js;
    if( !g_pJoystick )
        return;
    hr = g_pJoystick->Poll();
    if( FAILED( hr ) )
    {
        qDebug() << "g_pJoystick->Poll() failed:" << diutils::toString(hr);
        hr = g_pJoystick->Acquire();
        while( hr == DIERR_INPUTLOST ) {
            qDebug() << "g_pJoystick->Acquire() failed:" << diutils::toString(hr);
            hr = g_pJoystick->Acquire();
        }
        return;
    }
    if( FAILED( hr = g_pJoystick->GetDeviceState( sizeof( DIJOYSTATE2 ), &js ) ) ) {
        qDebug() << "g_pJoystick->GetDeviceState() failed:" << diutils::toString(hr);
        return;
    }
    float value = 0;
    int slider = 0;
    int pov = 0;
    for (auto axisid = 0; axisid < m_axisCount; axisid++)
    {
//        qDebug("Reading joystick axis %i of %i, %i", axisid, m_axisCount, m_DIaxisGIIDs.count());
        //Q_ASSERT(axisid<DIaxisGIIDs.count());
        GUID guidForThisAxis = m_DIaxisGIIDs.at(axisid);
        if (guidForThisAxis==GUID_XAxis)
            value = (float)(js.lX);
        else if(guidForThisAxis==GUID_YAxis)
            value = (float)(js.lY);
        else if(guidForThisAxis==GUID_ZAxis)
            value = (float)(js.lZ);
        else if(guidForThisAxis==GUID_RxAxis)
            value = (float)(js.lRx);
        else if(guidForThisAxis==GUID_RyAxis)
            value = (float)(js.lRy);
        else if(guidForThisAxis==GUID_RzAxis)
            value = (float)(js.lRz);
        else if(guidForThisAxis==GUID_Slider)
        {
            value = (float)(js.rglSlider[slider]);
            slider++;
        }
        //POV
        if(guidForThisAxis==GUID_POV)
        {
            DWORD povvalue = js.rgdwPOV[pov];
            if ((float)(povvalue)!=m_axisValues.value(axisid))
            {
                m_axisValues.insert(axisid,value);
                emit axisValueChanged(m_deviceInstanceId,axisid,(float)(LOWORD(povvalue)));
            }
//            axisid++;
            pov++;
        }
        else if(value!=m_axisValues.value(axisid))
        {
            m_axisValues.insert(axisid,value);
            emit axisValueChanged(m_deviceInstanceId,axisid,value);
        }
    }
    //Buttons
    for( auto i = 0; i < m_buttonsCount; i++ )
    {
        if( js.rgbButtons[i] & 0x80 )
        {
           if (m_buttonValues.value(i)!=true)
           {
               m_buttonValues.insert(i,true);
//                   qDebug("Button %i pressed.", i);
               emit buttonPressed(m_deviceInstanceId,i,true);
           }
        }else
        {
            if (m_buttonValues.value(i)!=false)
            {
                m_buttonValues.insert(i,false);
//                    qDebug("Button %i released.", i);
                emit buttonPressed(m_deviceInstanceId,i,false);
            }
        }
    }
}


void DirectInputGameController::onButtonPressed(GUID controllerId, uint btnId, bool bPressed)
{
    qDebug()<<"onButtonPressed -" <<"btnId:"<<btnId<<"bPressed:"<<bPressed;
}

void DirectInputGameController::onAxisValueChanged(GUID controllerId, uint axisId, float value)
{
    qDebug()<<"onAxisValueChanged -"<<"axisId:"<<axisId<<"value:"<<value;
}

BOOL CALLBACK EnumObjectsCallback(const DIDEVICEOBJECTINSTANCE* pdidoi, VOID* pContext)
{
    //Set the range for axis
    DirectInputGameController* pDIGC=(DirectInputGameController*)pContext;
    if( pdidoi->dwType & DIDFT_AXIS )
    {
        DIPROPRANGE diprg;
        diprg.diph.dwSize = sizeof( DIPROPRANGE );
        diprg.diph.dwHeaderSize = sizeof( DIPROPHEADER );
        diprg.diph.dwHow = DIPH_BYID;
        diprg.diph.dwObj = pdidoi->dwType; // Specify the enumerated axis
        diprg.lMin = -1000;
        diprg.lMax = +1000;
        if( FAILED( pDIGC->g_pJoystick->SetProperty( DIPROP_RANGE, &diprg.diph ) ) )
            return DIENUM_STOP;
    }
//    qDebug() << "JSObject Found";

    // Set the UI to reflect what objects the joystick supports
    if( pdidoi->guidType == GUID_XAxis )
    {
        pDIGC->m_DIaxisGIIDs.push_back(pdidoi->guidType);

//        qDebug() << "Axis found GUID_XAxis";
        pDIGC->m_axisCount++;
    }
    if( pdidoi->guidType == GUID_YAxis )
    {
        pDIGC->m_DIaxisGIIDs.push_back(pdidoi->guidType);
//        qDebug() << "Axis found GUID_YAxis";
        pDIGC->m_axisCount++;
    }
    if( pdidoi->guidType == GUID_ZAxis )
    {
        pDIGC->m_DIaxisGIIDs.push_back(pdidoi->guidType);
//        qDebug() << "Axis found GUID_ZAxis";
        pDIGC->m_axisCount++;
    }
    if( pdidoi->guidType == GUID_RxAxis )
    {
        pDIGC->m_DIaxisGIIDs.push_back(pdidoi->guidType);
//        qDebug() << "Axis found GUID_RxAxis";
        pDIGC->m_axisCount++;
    }
    if( pdidoi->guidType == GUID_RyAxis )
    {
        pDIGC->m_DIaxisGIIDs.push_back(pdidoi->guidType);
//        qDebug() << "Axis found GUID_RyAxis";
        pDIGC->m_axisCount++;
    }
    if( pdidoi->guidType == GUID_RzAxis )
    {
        pDIGC->m_DIaxisGIIDs.push_back(pdidoi->guidType);
//        qDebug() << "Axis found GUID_RzAxis";
        pDIGC->m_axisCount++;
    }
    if( pdidoi->guidType == GUID_Slider )
    {
//        qDebug() << "Slider found GUID_Slider";
        pDIGC->m_DIaxisGIIDs.push_back(pdidoi->guidType);
        pDIGC->m_axisCount++;
    }
    if( pdidoi->guidType == GUID_POV )
    {
//        qDebug() << "POV found GUID_POV";
        //We add two axis to represent this pov
        pDIGC->m_DIaxisGIIDs.push_back(pdidoi->guidType);
        pDIGC->m_axisCount++;
//        pDIGC->m_DIaxisGIIDs.push_back(pdidoi->guidType);
//        pDIGC->m_axisCount++;
    }
    if( pdidoi->guidType == GUID_Button )
    {
//        qDebug() << "Button found GUID_Button";
        pDIGC->m_buttonsCount++;
    }
    if( pdidoi->guidType == GUID_Key )
    {
//        qDebug() << "Key found GUID_Key";
    }
    if( pdidoi->guidType == GUID_Unknown )
    {
//        qDebug() << "Unknown object found";
    }

//    qDebug() << "pDIGC->m_buttonsCount:" << pDIGC->m_buttonsCount << "pDIGC->m_axisCount:" << pDIGC->m_axisCount;

    return DIENUM_CONTINUE;

}

void DirectInputGameController::update_this_shit()
{
    ReadJoystick();
}
