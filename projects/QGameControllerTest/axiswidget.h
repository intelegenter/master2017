#ifndef AXISWIDGET_H
#define AXISWIDGET_H

#include <QWidget>

namespace Ui {
class AxisWidget;
}

class AxisWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AxisWidget(QWidget *parent = 0);
    ~AxisWidget();


    uint getAxisId() const;
    void setAxisId(const uint &value);

    double getAxisValue() const;
    void setAxisValue(double value);

private:
    Ui::AxisWidget *ui;
    uint axisId;
    double axisValue;
};

#endif // AXISWIDGET_H
