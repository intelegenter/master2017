#include "axiswidget.h"
#include "ui_axiswidget.h"

AxisWidget::AxisWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AxisWidget)
{
    ui->setupUi(this);
}

AxisWidget::~AxisWidget()
{
    delete ui;
}

uint AxisWidget::getAxisId() const
{
    return axisId;
}

void AxisWidget::setAxisId(const uint &value)
{
    axisId = value;
    ui->axisIdLabel->setText(QString::number(axisId));
}

double AxisWidget::getAxisValue() const
{
    return axisValue;
}

void AxisWidget::setAxisValue(double value)
{
    //value = 1000;
    axisValue = value;

    if(ui->valueSlider->minimum() >= value)
        ui->valueSlider->setMinimum(value);
    if(ui->valueSlider->maximum() <= value)
        ui->valueSlider->setMaximum(value);

    ui->minValueLabel->setText(QString::number(ui->valueSlider->minimum()));
    ui->maxValueLabel->setText(QString::number(ui->valueSlider->maximum()));

    ui->valueSlider->setValue(value);
    ui->valueSpin->setValue(value);
}
