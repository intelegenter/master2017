#ifndef DIRECTINPUTGAMECONTROLLER_H
#define DIRECTINPUTGAMECONTROLLER_H

#include <QObject>
#include <QList>

#include <windows.h>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include <dinputd.h>
//struct DI_ENUM_CONTEXT
//{
//    DIJOYCONFIG* pPreferredJoyCfg;
//    bool bPreferredJoyCfgValid;
//};
BOOL CALLBACK EnumObjectsCallback( const DIDEVICEOBJECTINSTANCE* pdidoi, VOID* pContext );

class DirectInputGameController : public QObject
{
    Q_OBJECT
public:
    struct JoystickDecl {
        GUID id;
        QString name;
        QString description;
    };
    static QList<DirectInputGameController::JoystickDecl> EnumJoysticks();
public:
    explicit DirectInputGameController(QObject *parent = 0);
    ~DirectInputGameController();

    void create(GUID devInstanceId);
    void release();

    uint axisCount();
    uint buttonCount();
    float axisValue(uint axisId);
    bool buttonValue(uint buttonId);
    QString description();
    uint deviceId();
    bool isValid();
    void ReadJoystick();
    //
signals:
    void buttonPressed(GUID controllerId, uint btnId, bool bPressed);
    void axisValueChanged(GUID controllerId, uint axisId, float value);
public slots:
    void update_this_shit();
    void onButtonPressed(GUID controllerId, uint btnId, bool bPressed);
    void onAxisValueChanged(GUID controllerId, uint axisId, float value);
private:
    friend BOOL CALLBACK EnumObjectsCallback( const DIDEVICEOBJECTINSTANCE* pdidoi, VOID* pContext );
    LPDIRECTINPUTDEVICE8    g_pJoystick = nullptr;
    LPDIRECTINPUT8          g_pDI = nullptr;
    quint32 m_axisCount;
    quint32 m_buttonsCount;
    GUID m_deviceInstanceId;
//public:
    QMap<uint, float> m_axisValues;
    QMap<uint, bool> m_buttonValues;
    QList<GUID> m_DIaxisGIIDs;
    QTimer* m_timer = nullptr;
};

#endif // DIRECTINPUTGAMECONTROLLER_H
